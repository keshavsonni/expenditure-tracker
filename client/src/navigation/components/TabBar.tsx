import React, { FunctionComponent } from 'react';
import PropTypes from 'prop-types';
import { stringifyClassMap } from '../../shared/utils/style';

import '../styles/TabBar.scss';

export interface Tab {
  id: string;
  name: string;
}

export interface TabBarProps {
  tabs: Tab[];
  currentTab: string;
  onTabClick: (id: string) => void;
}

// Display product icon
const TabBar: FunctionComponent<TabBarProps> = props => {
  
  const tabClasses = function(tab: Tab): string {
    return stringifyClassMap({
      'tab-bar__tab': true,
      'tab-bar__tab--inactive': tab.id !== props.currentTab
    });
  }
  
  const tabs = () => {
    const tabsList: JSX.Element [] = [];
    for (const tab of props.tabs) {
      tabsList.push(
        <div key={tab.id} className={tabClasses(tab)} 
             onClick={() => props.onTabClick(tab.id)}>{ tab.name }</div>
      );
    }
    return tabsList;
  }

  return (
    <div className="tab-bar">
      {tabs()}
    </div>
  );
};

TabBar.propTypes = {
  ...TabBar.propTypes,
  currentTab: PropTypes.string.isRequired,
  tabs: PropTypes.array.isRequired,
  onTabClick: PropTypes.func.isRequired
};

export default TabBar;
