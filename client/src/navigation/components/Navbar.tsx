import React, { FunctionComponent } from 'react';
import PropTypes from 'prop-types';

import Card from '../../shared/components/Card';
import Icon, { IconSize } from '../../shared/components/Icon';

import '../styles/Navbar.scss';
import config from '../../config';
import { isMobile } from 'react-device-detect';

export interface NavbarProps {
  title: string;
}

// Display product icon
const Navbar: FunctionComponent<NavbarProps> = props => {
  const iconSize = function(): IconSize {
    if (isMobile) {
      return 'small';
    }
    return 'medium';
  }

  return (
    <div>
      <Card sharpCorners noBorders>
        <section className="navbar">
          <span className="navbar__logo">
            <Icon iconKey="logo" />
          </span>
          <span className="navbar__title"> {props.title}</span>

          <span className="navbar__social-container">
            <a href={config.social.linkedIn} target="__blank" className="navbar__social-icon">
              <Icon iconSize={iconSize()} clickable iconKey="linkedIn" />
            </a>
            
            <a href={config.social.gmail} target="__blank" className="navbar__social-icon">
              <Icon iconSize={iconSize()} clickable iconKey="gmail" />
            </a>

            <a href={config.social.github} target="__blank" className="navbar__social-icon">
              <Icon iconSize={iconSize()} clickable iconKey="github" />
            </a>
          </span>
        </section>
      </Card>
    </div>
  );
};

Navbar.propTypes = {
  ...Navbar.propTypes,
  title: PropTypes.string.isRequired
};

export default Navbar;
