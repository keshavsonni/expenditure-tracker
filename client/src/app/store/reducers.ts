import { AppActionType } from './constants';
import { AppState, AppAction } from './types';
import { size } from '../../shared/utils/utils';


export const initialState: AppState = {
  user: undefined,
  authRequestStatus: 'none',
  userRequestStatus: 'none',
  document: {
    size: size(0,0)
  }
};

const reducer = (state: AppState = initialState, action: AppAction): AppState => {
  switch(action.type) {
    case AppActionType.setUser:
      return {
        ...state,
        user: action.user
      };

    case AppActionType.setAuthRequestStatus:
      return {
        ...state,
        authRequestStatus: action.status 
      };

    case AppActionType.setUserRequestStatus:
      return {
        ...state,
        userRequestStatus: action.status
      };
    
    case AppActionType.setDocumentSize:
      return {
        ...state,
        document: {
          ...state.document,
          size: action.size
        }
      }

    default: 
      return state;
  }
};

export default reducer;
