import { AppActionType } from "./constants";
import { User } from "../../shared/models/entities";
import { RequestStatus } from "../../shared/models/requests";
import { Size } from "../../shared/utils/utils";

export interface AppState {
    user?: User;
    authRequestStatus: RequestStatus;
    userRequestStatus: RequestStatus;
    document: {
        size: Size; 
    };
}

export interface BaseAppAction {
    type: AppActionType;
}

export interface SetUserAction extends BaseAppAction {
    type: AppActionType.setUser;
    user: User;
}

export interface SetAuthRequestStatusAction extends BaseAppAction {
    type: AppActionType.setAuthRequestStatus;
    status: RequestStatus;
}

export interface SetUserRequestStatusAction extends BaseAppAction {
    type: AppActionType.setUserRequestStatus;
    status: RequestStatus;
}

export interface SetDocumentSizeAction extends BaseAppAction {
    type: AppActionType.setDocumentSize;
    size: Size;
}

export type AppAction = SetUserAction | SetAuthRequestStatusAction | SetUserRequestStatusAction | SetDocumentSizeAction;