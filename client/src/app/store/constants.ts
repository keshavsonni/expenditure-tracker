export enum AppActionType {
  setUser = 'APP/SET_USER',
  setAuthRequestStatus = 'APP/SET_AUTH_REQUEST_STATUS',
  setUserRequestStatus = 'APP/SET_USER_REQUEST_STATUS',
  setDocumentSize = 'APP/SET_DOCUMENT_SIZE'
}
