import { User } from "../../shared/models/entities";
import { AppActionType } from "./constants";
import { AppAction } from "./types";
import { AppThunk } from "../../store/types";
import axios from 'axios';
import config from '../../config';
import base64 from 'base-64';
import { RequestStatus } from "../../shared/models/requests";
import { Size } from "../../shared/utils/utils";

export const setDocumentSize = function(size: Size): AppAction {
    return {
        type: AppActionType.setDocumentSize,
        size
    };
}

export const setUser = function(user: User): AppAction {
    return {
        type: AppActionType.setUser,
        user: user
    }
}

export const setAuthRequestStatus = function(status: RequestStatus): AppAction {
    return {
        type: AppActionType.setAuthRequestStatus,
        status: status
    }
}

export const setUserRequestStatus = function(status: RequestStatus): AppAction {
    return {
        type: AppActionType.setUserRequestStatus,
        status: status
    }
}

// Loads the current user's account once authenticated
export const fetchUser = (): AppThunk => async dispatch => {
    dispatch(setUserRequestStatus('loading'));
    try {
        const res = await axios.get(`${config.apiUrl}/user`, {
            withCredentials: true
        });
        const user = res.data as User;
        dispatch(setUserRequestStatus('success'));
        dispatch(setUser(user));
    } catch (e) {
        console.error(e);
        dispatch(setUserRequestStatus('failed'));
    }
}

// Perform authentication request
export const doLogin = (username: string, password: string): AppThunk => async dispatch => {
    dispatch(setAuthRequestStatus('loading'));
    await axios.post(`${config.apiUrl}/logout`); // Clear any old cookies
    try {
        const token = base64.encode(username  + ':' + password);
        await axios.post(`${config.apiUrl}/login`, null, {
            headers: {
                'Authorization' : `Basic ${token}`
            },
            withCredentials: true
        });
        dispatch(setAuthRequestStatus('success'));
        dispatch(fetchUser());
    } catch (e) {
        console.error(e);
        dispatch(setAuthRequestStatus('failed'));
    }
}