import { User } from '../../shared/models/entities';
import { RootState } from '../../store/types';
import { RequestStatus } from '../../shared/models/requests';
import { Size } from '../../shared/utils/utils';

export const authRequestStatus = (state: RootState): RequestStatus => {
    return state.app.authRequestStatus;
}

export const userRequestStatus = (state: RootState): RequestStatus => {
    return state.app.userRequestStatus;
}

export const user = (state: RootState): User | undefined => {
    return state.app.user;
}

export const documentSize = (state: RootState): Size => {
    return state.app.document.size;
}