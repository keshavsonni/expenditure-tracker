import { FunctionComponent, useEffect } from "react";
import { useDispatch } from "react-redux";
import { setDocumentSize } from "../store/action-creators";
import { size } from "../../shared/utils/utils";

const DocumentEventManager: FunctionComponent = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        const setDomSize = () => {
            const width = window.innerWidth;
            const height = window.innerHeight;
            const docSize = size(width,height);
            dispatch(setDocumentSize(docSize));
        }

        window.addEventListener('resize', setDomSize);
        setDomSize();
    }, [dispatch])

    return null;
}

export default DocumentEventManager;