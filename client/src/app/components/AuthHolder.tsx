import React, { FunctionComponent, useEffect } from 'react';

import DashboardHolder from './DashboardHolder';
import Auth from './Auth';
import { authRequestStatus } from '../store/selectors';
import { doLogin } from '../store/action-creators';
import { useDispatch, useSelector } from 'react-redux';
import config from '../../config';

const AuthHolder: FunctionComponent = () => {
  const dispatch = useDispatch();
  const authStatus = useSelector(authRequestStatus)

  useEffect(() => {
    // Fetch token from the URL and attempt auth
    const search: string = window.location.search;
    const params = new URLSearchParams(search);
    const token = params.get('token') ?? '';
    dispatch(doLogin(config.guestAccount, token));
  }, [dispatch])

  if(authStatus === 'success') {
    // Show dashboard on successful auth
    return <DashboardHolder />;
  } else {
    // Loading/error screen
    return <Auth authRequestStatus={authStatus} />
  }
};

export default AuthHolder;
