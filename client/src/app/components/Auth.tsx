import React, { FunctionComponent } from 'react';
import Spinner from '../../shared/components/Spinner';
import PropTypes from 'prop-types';

import '../styles/Auth.scss';
import { useLoadingText } from '../../shared/hooks/utils';
import { RequestStatus } from '../../shared/models/requests';

export interface AuthProps {
  authRequestStatus: RequestStatus;
}

const Auth: FunctionComponent<AuthProps> = props => {
  const loginText = useLoadingText('Logging in');

  /* Display spinner and a 401 if auth fails */
  const authComponent = function(): JSX.Element {
    if(props.authRequestStatus === 'failed') {
      return (
        <div className="auth__failure-indicator">
          Error 401: The access token is either invalid or has expired
        </div>
      )
    } else {
      return (
        <div>
          <div className="auth__spinner-holder">
            <Spinner size="large" /> 
          </div>
      
          <div className="auth__login-text">{loginText}</div>
        </div>
      )
    }
  }

  return (
    <div className="auth">
      { authComponent() }
    </div>
  );
};

Auth.propTypes = {
  ...Auth.propTypes,
  authRequestStatus: PropTypes.oneOf<RequestStatus>(['loading', 'failed', 'none']).isRequired
};

export default Auth;
