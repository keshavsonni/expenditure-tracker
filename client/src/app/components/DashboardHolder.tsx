import React, { FunctionComponent, useEffect } from 'react';

import TableHolder from '../../table/components/TableHolder';

import '../styles/DashboardHolder.scss';
import InfoCard from '../../info-card/components/InfoCard';
import { useDispatch, useSelector } from 'react-redux';
import { fetchTransactionCategories } from '../../table/store/action-creators';
import { categoriesStatusSelector } from '../../table/store/selectors';

const DashboardHolder: FunctionComponent = () => {

  const dispatch = useDispatch();
  useEffect(() => {
    // We need this to do anything with tables or filters
    dispatch(fetchTransactionCategories());
  }, [dispatch]);

  const categoryStatus = useSelector(categoriesStatusSelector);

  return (
    <div className="dashboard-holder">
      {
        categoryStatus === 'success' ? 
        (
          <div className="dashboard-holder__transactions">
            <div className="dashboard-holder__info-card">
              <InfoCard />
            </div>
            <div className="dashboard-holder__records">
              <TableHolder />
            </div>
          </div>
        ) : ''
      }    
    </div>
  );
};

export default DashboardHolder;
