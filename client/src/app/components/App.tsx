import React, { FunctionComponent } from 'react';

import Navbar from '../../navigation/components/Navbar';
import AuthHolder from './AuthHolder';
import DocumentEventManager from './DocumentEventManager';

import '../styles/App.scss';

const App: FunctionComponent = () => {
  return (
    <div className="app">
      <DocumentEventManager />

      <Navbar title="Expenditure tracker" />
      
      <AuthHolder /> 
    </div>
  );
};

export default App;
