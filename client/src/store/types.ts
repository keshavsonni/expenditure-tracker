import { AppState } from "../app/store/types";
import { TableState } from "../table/store/types";
import { InfoCardState } from "../info-card/store/types";
import { ThunkAction } from "redux-thunk";
import { Action } from "redux";

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>

export interface RootState {
    app: AppState;
    table: TableState;
    infoCard: InfoCardState;
}