import { combineReducers } from 'redux';

import table, { initialState as tableState } from '../table/store/reducers';
import app, { initialState as appState } from '../app/store/reducers';
import infoCard, { initialState as infoCardState } from '../info-card/store/reducers';
import { RootState } from './types';

export const initialState: RootState = {
  table: tableState,
  app: appState,
  infoCard: infoCardState
};

const rootReducer = combineReducers({
  app,
  table,
  infoCard
});

export default rootReducer;
