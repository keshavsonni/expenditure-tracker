import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer, { initialState } from '../store/rootReducer';

const storeCreator = applyMiddleware(thunkMiddleware)(createStore);

export default function setupStore() {
  return storeCreator(rootReducer, initialState);
}
