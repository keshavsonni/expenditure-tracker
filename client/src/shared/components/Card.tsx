import React, { FunctionComponent } from 'react';
import PropTypes from 'prop-types';

import '../styles/Card.scss';
import { stringifyClassMap } from '../utils/style';

export interface CardProps {
  sharpCorners?: boolean;
  noShadows?: boolean;
  noBorders?: boolean;
  grow?: boolean;
}

// A card component that provides some shadow visuals
const Card: FunctionComponent<CardProps> = props => {
  const classes = (): string => {
    return stringifyClassMap({
      'card': true,
      'card--rounded': !props.sharpCorners,
      'card--shadow': !props.noShadows,
      'card--borders': !props.noBorders,
      'card--grow': !!props.grow
    });
  };

  return <div className={classes()}>{props.children}</div>;
};

Card.propTypes = {
  ...Card.propTypes,
  noBorders: PropTypes.bool,
  noShadows: PropTypes.bool,
  sharpCorners: PropTypes.bool
};

export default Card;
