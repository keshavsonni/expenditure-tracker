import React, { FunctionComponent } from 'react';
import PropTypes from 'prop-types';

import '../styles/Button.scss'
import { stringifyClassMap } from '../utils/style';

export type ButtonShape = 'circle' | 'rect' | 'capsule';
export type ButtonTheme = 'default' | 'confirm';

export interface ButtonProps {
    shape?: ButtonShape;
    theme?: ButtonTheme;
    onClick?: () => void;
    disabled?: boolean;
}

// A generic button with different configuration options
const Button: FunctionComponent<ButtonProps> = (props) => {
  
  const classes = (): string => {
      return stringifyClassMap({
          'button': true,
          'button--circle': props.shape === 'circle',
          'button--rect': props.shape == null || props.shape === 'rect',
          'button--capsule': props.shape === 'capsule',
          'button--confirm-theme': props.theme === 'confirm',
          'button--default-theme': props.theme == null || props.theme === 'default',
          'button--disabled': !!props.disabled
      })
  }  

  return (
    <div onClick={props.onClick} className={classes()} >{ props.children }</div>
  );
};

Button.propTypes = {
    ...Button.propTypes,
    onClick: PropTypes.func,
    shape: PropTypes.oneOf<ButtonShape>(['circle', 'rect', 'capsule']),
    theme: PropTypes.oneOf<ButtonTheme>(['confirm', 'default'])
};

export default Button;
