import React, { FunctionComponent } from "react";
import Card from "./Card";
import Button from "./Button";
import PropTypes from 'prop-types';
import Icon from './Icon';

import '../styles/Dialog.scss';

export interface DialogAction {
    text: string;
    action: () => void;
    disabled?: boolean;
}

export interface DialogProps {
    title: string;
    // If cancel and confirm actions are provided, 
    // buttons will be automatically shown
    cancel?: DialogAction;
    confirm?: DialogAction;
    delete?: DialogAction;
}

// A gneric dialog to be used for different actions
const Dialog: FunctionComponent<DialogProps> = (props) => {
    
    // Decides which buttons to show in the footer
    const constructFooter = function(): JSX.Element | undefined {
        const buttons: JSX.Element[] = [];
        if(props.cancel) {
            buttons.push(
                <Button disabled={!!props.cancel.disabled}  
                        key='cancel' 
                        onClick={props.cancel.action}>
                    {props.cancel.text}
                </Button>
            );
        }

        if(props.confirm) {
            buttons.push(
                <Button disabled={!!props.confirm.disabled} 
                        key='confirm' onClick={props.confirm.action} 
                        theme='confirm' >
                    {props.confirm.text}
                </Button>
            );
        }

        if (buttons.length === 0) {
            return undefined;
        } 

        return(
            <div className="dialog__footer">
                {buttons}
            </div>
        )
    }

    return (
        <div className="dialog__overlay">
            <div className="dialog__container">
                <Card noShadows grow>
                    <div className="dialog">
                        <div className="dialog__title">
                            <div>{props.title}</div>
                            {   props.delete ?   
                                <div className="dialog__delete-button">
                                    <Button shape="circle" onClick={ props.delete?.action }><Icon iconKey='deleteBin' /></Button>
                                </div> : null
                            }
                        </div>
                        {props.children}
                        {constructFooter()}
                    </div>
                </Card>
            </div>
        </div>
    );
};

Dialog.propTypes = {
    ...Dialog.propTypes,
    title: PropTypes.string.isRequired,
    cancel: PropTypes.shape({
        text: PropTypes.string.isRequired,
        action: PropTypes.func.isRequired,
        disabled: PropTypes.bool
    }),
    confirm: PropTypes.shape({
        text: PropTypes.string.isRequired,
        action: PropTypes.func.isRequired,
        disabled: PropTypes.bool
    }),
    delete: PropTypes.shape({
        text: PropTypes.string.isRequired,
        action: PropTypes.func.isRequired,
        disabled: PropTypes.bool
    })
}

export default Dialog;