import React, { useState } from 'react';
import { EntityWithId } from '../models/entities';

import '../styles/Dropdown.scss'
import Icon, { IconKey } from './Icon';
import { stringifyClassMap } from '../utils/style';

export interface DropdownProps<T> {
    items: T[]; // Options
    displayKey: keyof T; // The key in the object to display
    value: T;
    setValue: (v: T) => void;
}

// Allows user to pick an option from a drop doen list
const Dropdown = <T extends EntityWithId> (props: DropdownProps<T>): JSX.Element => {
    const [dropdownOpen, openDropdown] = useState(false);

    //  Render options
    const options = function(): JSX.Element[] {
        const rows: JSX.Element[] = [];
        for (const item of props.items) {
            if(item.id === props.value?.id) {
                continue;
            }
            rows.push(
                <div key={item.id} 
                           onClick={() => props.setValue({...item})} 
                           className="dropdown__item-row">
                               <span className="dropdown__item-text">{item[props.displayKey]}</span>
                </div>
            )
        }

        return rows;
    }

    // CSS classes to apply based on state
    const dropdownClasses = stringifyClassMap({
        'dropdown--shadow': dropdownOpen,
        'dropdown': true
    });

    const dropdownContainerClasses = stringifyClassMap({
        'dropdown__container' : true,
        'dropdown__container--lift': dropdownOpen
    })

    const decideIconKey = function(): IconKey {
        return dropdownOpen ? 'arrowUp' : 'arrowDown';
    }

    // Show/Hide dropdown
    const toggleDropdown = function() {
        openDropdown(!dropdownOpen);
    }

    const selectedDisplayValue = function(): T[keyof T] | undefined {
        return props.value ? props.value[props.displayKey] : undefined
    }

    return (
        <div className={dropdownContainerClasses} >
            <div className={dropdownClasses} onClick={toggleDropdown} onBlur={() => openDropdown(false)} tabIndex={0}>
                { <div className="dropdown__item-row">
                    <span className="dropdown__item-text">{selectedDisplayValue()}</span>  
                    <span className="dropdown__item-icon"><Icon iconKey={decideIconKey()} /></span>
                </div> }
                { dropdownOpen ? options() : '' }
            </div>
        </div>
    );
};

export default Dropdown;
