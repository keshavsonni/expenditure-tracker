import React, { FunctionComponent, ChangeEvent } from 'react';
import { stringifyClassMap } from '../utils/style';
import PropTypes from 'prop-types';

import '../styles/TextInput.scss'

export interface TextInputProps {
    type?: string;
    maxLen?: number;
    value: string | number;
    onDataChange: (val: string | number | undefined) => void; 
    error?: boolean;
    hint?: string;
    onBlur?: () => void;
}

// Allows user to inter text input. Numeric or string
const TextInput: FunctionComponent<TextInputProps> = (props) => {

    // Changing presentation based on state
    const classes = (): string => {
        return stringifyClassMap({
            'text-input': true,
            'text-input--error': !!props.error
        });
    }

    const onChange = (e: ChangeEvent<HTMLInputElement>) => {
        if(e.target.value != null) {
            if(props.type === 'number') {
                if(isNaN(e.target.valueAsNumber)) {
                    props.onDataChange(e.target.value);
                    return;
                }
                props.onDataChange(e.target.valueAsNumber);
            } else {
                props.onDataChange(e.target.value);
            }
        }
    }

    return (
        <input className={classes()} onBlur={props.onBlur}
             type={props.type} placeholder={props.hint}
             maxLength={props.maxLen} value={props.value} onChange={onChange} />
    );
};

TextInput.propTypes = {
    ...TextInput.propTypes,
    type: PropTypes.string,
    maxLen: PropTypes.number,
    value: PropTypes.any.isRequired,
    onDataChange: PropTypes.func.isRequired,
    error: PropTypes.bool,
    hint: PropTypes.string,
    onBlur: PropTypes.func
}

export default TextInput;
