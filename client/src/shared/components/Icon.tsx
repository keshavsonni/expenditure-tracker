import React, { FunctionComponent } from 'react';
import PropTypes from 'prop-types';

import logo from '../assets/images/logo.svg';
import arrowUp from '../assets/images/chevron-up.png';
import arrowDown from '../assets/images/chevron-down.png';
import deleteBin from '../assets/images/delete-16.png';
import linkedIn from '../assets/images/linkedin.png';
import github from '../assets/images/github.png';
import gmail from '../assets/images/gmail.png';
import { pixelsToRem, stringifyClassMap } from '../utils/style';
import '../styles/Icon.scss';

export type IconKey = 'logo' | 'arrowUp' | 'arrowDown' | 'deleteBin' | 'linkedIn' | 'github' | 'gmail';

export type IconSize = 'small' | 'medium' | 'large';

export interface IconProps {
  iconKey: IconKey;
  iconSize?: IconSize;
  clickable?: boolean;
  onClick?: () => void;
}

// Simplifies showing icons. Helps avoid importing assets every time
const icons: Record<IconKey, string> = {
  logo,
  arrowUp,
  arrowDown,
  deleteBin,
  linkedIn,
  github,
  gmail
} as const;

const Icon: FunctionComponent<IconProps> = props => {
  const iconStyle = function(): React.CSSProperties {
    let maxWidth = 32;
    if (props.iconSize === 'large') {
      maxWidth = 64;
    } else if (props.iconSize === 'small') {
      maxWidth = 24;
    }
    const maxWidthRem = pixelsToRem(maxWidth) + 'rem';
    return {
      maxWidth: maxWidthRem 
    }
  }

  const classes = function(): string {
    return stringifyClassMap({
      'icon--clickable' : !!props.clickable
    })
  }

  return <img className={classes()} style={iconStyle()} src={icons[props.iconKey]} onClick={props.onClick} alt={props.iconKey}></img>;
};

Icon.propTypes = {
  ...Icon.propTypes,
  iconKey: PropTypes.oneOf<IconKey>(Object.keys(icons) as IconKey[]).isRequired
};

export default Icon;
