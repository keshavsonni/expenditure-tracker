import React, { FunctionComponent } from 'react';
import PropTypes from 'prop-types';

import '../styles/Spinner.scss'
import { stringifyClassMap } from '../utils/style';

export type SpinnerSize = 'small' | 'large';

export interface SpinnerProps {
  size: SpinnerSize;
}
// A loading spinner support small and large size configs
const Spinner: FunctionComponent<SpinnerProps> = props => {

  const classes = function(): string {
    return stringifyClassMap({
      'spinner' : true,
      'spinner--large': props.size === 'large',
      'spinner--small' : props.size === 'small'
    });
  }

  return (
      <div className={classes()} />
  );
};

Spinner.propTypes = {
  ...Spinner.propTypes,
  size : PropTypes.oneOf<SpinnerSize>(['small', 'large']).isRequired
};

export default Spinner;
