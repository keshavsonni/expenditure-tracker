import { useState, useEffect, useRef } from 'react';

// Hook to animate the 3 dots in texts like 'Loding...'
export const useLoadingText = function(initialText: string): string {
    const [text] = useState<string>(initialText);
    const [dotsCount, setDotsCount] =  useState<number>(0);

    useEffect(() => {
        const id = setTimeout(() => {
            if(dotsCount >= 3) {
                setDotsCount(0);
            } else {
                setDotsCount(dotsCount + 1);
            }
        }, 800);

        return function cleanup() {
            clearTimeout(id);
        }    
    }, [dotsCount]);

    let dots = '';
    let i = 0;
    while (i < dotsCount) {
        i ++;
        dots = dots + '.'; 
    }
    return text + dots;
}

export const usePrevious = function<T>(value: T) {
    const ref = useRef<T>();
    useEffect(() => {
        ref.current = value;
    });
    return ref.current;
}