import { useSelector, useDispatch } from "react-redux"
import { totalPagesSelector, transactionsFilterSelector } from "../../table/store/selectors"
import { TransactionsFilter } from "../models/entities";
import { fetchTransactions, setTransactionsFilter } from "../../table/store/action-creators";
import { useCallback } from "react";

// Ability to modify filtering
export const useTransactionsFiltering = (): [number, TransactionsFilter, (filter: TransactionsFilter) => void] => {
    const totalPages = useSelector(totalPagesSelector);
    const filter = useSelector(transactionsFilterSelector);
    const dispatch = useDispatch();

    const applyFilter = useCallback((newFilter: TransactionsFilter) => {
        dispatch(fetchTransactions(newFilter));
        dispatch(setTransactionsFilter(newFilter));
    }, [dispatch])

    return [totalPages, filter, applyFilter];
}