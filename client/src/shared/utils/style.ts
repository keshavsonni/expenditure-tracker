import React from 'react';

export const ellipsis: React.CSSProperties = {
  textOverflow: 'ellipsis',
  whiteSpace: 'nowrap',
  overflow: 'hidden'
};

// Css property to set width
export const percentWidth = function(percentage: number): React.CSSProperties {
  const computedPercentage =
    percentage > 0 ? Math.min(100, 100 * percentage) : 0;

  return {
    width: `${computedPercentage}%`
  };
};

// Takes a map of classess with true/false vallues and constructs a class list
export const stringifyClassMap = function(classMap: Record<string, boolean>): string {
  const classes: string [] = [];

  const classKeys = Object.keys(classMap);

  for (const k of classKeys) {
    if(classMap[k]) {
      classes.push(k);
    }
  }

  return classes.join(' ');
}

export const remToPixels = function(rem: number) {    
  return rem * parseFloat(getComputedStyle(document.documentElement).fontSize);
}

export const pixelsToRem = function(px: number) {
  return px / parseFloat(getComputedStyle(document.documentElement).fontSize);
}