import { shortenedNumString, BILLION, TRILLION } from "./number-format";

describe('Number formatter tests', () => {
    it('should handle single digit numbers', () => {
        const num = 1;
        const formatted = shortenedNumString(num);
        expect(formatted).toBe('1');
    });

    it('should handle double digit numbers', () => {
        const num = 45;
        const formatted = shortenedNumString(num);
        expect(formatted).toBe('45');
    });

    it('should handle 3 digit numbers', () => {
        const num = 300;
        const formatted = shortenedNumString(num);
        expect(formatted).toBe('300');
    });

    it('should handle 4 digit numbers', () => {
        const num = 1200;
        const formatted = shortenedNumString(num);
        expect(formatted).toBe('1.2k');
    });

    it('should handle whole 4 digit numbers', () => {
        const num = 5000;
        const formatted = shortenedNumString(num);
        expect(formatted).toBe('5k');
    });

    it('should handle large 4-5 digit numbers', () => {
        const num = 90801;
        const formatted = shortenedNumString(num);
        expect(formatted).toBe('90.8k');
    });

    it('should handle millions alright', () => {
        const num = 1234000;
        const formatted = shortenedNumString(num);
        expect(formatted).toBe('1.2M');
    });

    it('should handle billions alright', () => {
        const num = 12.343 * BILLION;
        const formatted = shortenedNumString(num);
        expect(formatted).toBe('12.3B');
    });

    it('should handle trillions alright', () => {
        const num = 12.343 * TRILLION;
        const formatted = shortenedNumString(num);
        expect(formatted).toBe('12.3T');
    });

    it('should just NaN very large numbers', () => {
        const num = 12.343 * TRILLION * 100;
        const formatted = shortenedNumString(num);
        expect(formatted).toBe('NaN');
    });
})