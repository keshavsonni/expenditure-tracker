import { EntityWithId } from '../models/entities';

// Converts an array to a HashMap like structure asuming the 'id' of each object is unique
export const arrayToMap = function<T extends EntityWithId>(entities: T[]): Record<string, T> {
  const map: Record<string, T> = {};
  for (const entity of entities) {
    map[entity.id] = entity;
  }
  return map;
};

// Opposite of the above function
export const mapToArray = function<T extends EntityWithId>(map: Record<string, T>): T[] {
  const ids = Object.keys(map);
  const ar: T[] = [];

  for(const id of ids) {
    ar.push(map[id]);
  }

  return ar;
}

export const isDev = function(): boolean {
  return process.env.NODE_ENV === 'development';
}

export const clip = function(val: number, min: number, max: number): number {
  if(val < min)
    return min;
  if(val > max)
    return max;
  return val;
}

export const constructGetQuery = function(queryObj: Record<string, any>) {
  const keys = Object.keys(queryObj);

  const parts: string[] = [];
  for (const key of keys) {
    if(queryObj[key] == null)
      continue;
    const encoded = encodeURIComponent(`${queryObj[key]}`);
    parts.push(`${key}=${encoded}`);
  }

  if(parts.length === 0) {
    return '';
  }

  return '?' + parts.join('&');
}

export const isEmpty = function(val: number | string | undefined | null): boolean {
  return val == null || val === '';
}

export interface Size { 
  width: number;
  height: number;
}

export const size = function(width: number, height: number): Size {
  return {width, height}; 
}

export interface Point {
  x: number;
  y: number;
}

export interface Rect {
  topLeft: Point;
  size: Size;
}