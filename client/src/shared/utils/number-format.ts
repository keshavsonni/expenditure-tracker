// Functions to generate shortened string representations of numbers using alphabetical suffixes
export const THOUSAND = 1000;
export const MILLION = THOUSAND * THOUSAND;
export const BILLION = MILLION * THOUSAND;
export const TRILLION = BILLION * THOUSAND;

interface BaseNumMap {
    k: number;
    M: number;
    B: number;
    T: number;
}

const baseNumMap: BaseNumMap = {
    k: THOUSAND,
    M: MILLION,
    B: BILLION,
    T: TRILLION
};

const numString = function (num: number, baseStr: keyof BaseNumMap): string {
    const base = baseNumMap[baseStr];
    if (base == null) {
        return NaN.toString();
    }
    const factor = num / base; 
    const deci = num % base !== 0 ? factor.toFixed(1) : Math.trunc(factor);
    return deci + baseStr;
}

export const shortenedNumString = function(val: number): string {
    if (val < 1) {
        return val.toFixed(2);
    }
    const num = Math.trunc(val);
    
    if (num < THOUSAND) {
        return `${num}`
    }

    if (num < MILLION) {
        return numString(num, 'k');
    }

    if (num < BILLION) {
        return numString(num, 'M');
    }

    if (num < TRILLION) {
        return numString(num, 'B');
    }

    if (num < TRILLION * THOUSAND) {
        return numString(num, 'T');
    }

    return NaN.toString();
}