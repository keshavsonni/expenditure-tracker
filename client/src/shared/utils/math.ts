import { Size, Point } from "../../shared/utils/utils";

// Converts from 0-1 system with origin on the bottom to pixels with origin on top
export const cartesianCoordToPixels = function(coord: Point, clientSize: Size): Point {
    const { width, height } = clientSize;
    const x = coord.x * width;
    const y = (1 - coord.y) * height;
    return { x, y };
}

export const cartesianCoordsToPixels = function<T extends Point>(coords: T[], clientSize: Size, clientOffset?: Point): T[] {
    return coords.map(p => {
        const { x, y } = p;
        const transformed = cartesianCoordToPixels({x,y}, clientSize);
        transformed.x += clientOffset?.x ?? 0;
        transformed.y += clientOffset?.y ?? 0;
        return {
            ...p,
            ...transformed
        };
    });
}