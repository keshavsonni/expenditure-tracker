// These are common models shared with the database
export interface EntityWithId {
  id: string;
}

export interface Transaction {
  id: string;
  createdAt: number;
  category: string;
  description?: string;
  value: number;
}

export interface TransactionCategory {
  id: string;
  name: string;
}

export interface GetTransactionsResponse {
  pages: number;
  result: Transaction[];
}

export type GetTransactionsCategoriesResponse = TransactionCategory[];

export interface TransactionsFilter {
  description?: string;
  from?: number;    // Start time
  to?: number;      // End time
  page?: number;
  results?: number;
  min?: number;     // Min value
  max?: number;     // Max value
  category?: string;
}

export interface User {
  id: string;
  name: string;
  email: string;
  password: string;
  createdAt: number;
  currency: string;
  transactions: Transaction[];
}

export type GetDailyReportResponse = Record<string, number>;

export type GetMonthlyReportResponse = Record<string, number>;