// Utility types for user requests
import PropTypes from 'prop-types';

export type RequestStatus = 'loading' | 'failed' | 'none' | 'success';

export const requestStatusValidator = PropTypes.oneOf<RequestStatus>(
    ['loading', 'failed', 'none', 'success']
).isRequired;