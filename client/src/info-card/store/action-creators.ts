import { GetDailyReportResponse, Transaction, TransactionsFilter, GetTransactionsResponse, GetMonthlyReportResponse } from "../../shared/models/entities";
import { InfoCardAction } from "./types";
import { InfoCardActionType } from "./constants";
import { RequestStatus } from "../../shared/models/requests";
import { AppThunk } from "../../store/types";
import moment from "moment";
import { constructGetQuery } from "../../shared/utils/utils";
import axios from "axios";
import config from "../../config";

export const setAnnualReport = function (report: GetMonthlyReportResponse): InfoCardAction {
    return {
        type: InfoCardActionType.setAnnualReport,
        report
    }
}

export const setMonthlyReport = function(report: GetDailyReportResponse): InfoCardAction {
    return {
        type: InfoCardActionType.setMonthlyReport,
        report
    }
}

export const setMonthlyReportStatus = function(status: RequestStatus): InfoCardAction {
    return {
        type: InfoCardActionType.setMonthlyReportStatus,
        status
    }
}

export const setTransactionsToday = function(transactions: Transaction[]): InfoCardAction {
    return {
        type: InfoCardActionType.setTransactionsToday,
        transactions
    }
}

export const setTransactionsTodayStatus = function(status: RequestStatus): InfoCardAction {
    return {
        type: InfoCardActionType.setTransactionsTodayStatus,
        status
    }
}

export const fetchTransactionsForToday = (): AppThunk => async dispatch => {
    dispatch(setTransactionsTodayStatus('loading'));

    const from = moment().utc().startOf('day').valueOf();
    const to = moment().utc().endOf('day').valueOf();
    const filter: TransactionsFilter = { from, to };
    const filterString = constructGetQuery(filter);
    
    try {
        const res = await axios.get(`${config.apiUrl}/transactions${filterString}`, {
            withCredentials: true
        });
        dispatch(setTransactionsTodayStatus('success'));
        const response: GetTransactionsResponse = res.data;
        dispatch(setTransactionsToday(response.result));
    } catch (e) {
        console.error(e);
        dispatch(setTransactionsTodayStatus('failed'));
    }
}

export const fetchMonthlyReport = (): AppThunk => async dispatch => {
    dispatch(setMonthlyReportStatus('loading'));

    const from = moment().utc().startOf('month').valueOf();
    const to = moment().utc().endOf('month').valueOf();
    const filter: TransactionsFilter = { from, to };
    const filterString = constructGetQuery(filter);

    try {
        const res = await axios.get(`${config.apiUrl}/dailyReport${filterString}`, {
            withCredentials: true
        });
        dispatch(setMonthlyReportStatus('success'));
        const response: GetDailyReportResponse = res.data;
        dispatch(setMonthlyReport(response));
    } catch (e) {
        console.error(e);
        dispatch(setMonthlyReportStatus('failed'));
    }
}

export const fetchAnnualReport = (): AppThunk => async dispatch => {
    try {
        const res = await axios.get(`${config.apiUrl}/monthlyReport`, {
            withCredentials: true
        });
        const response: GetMonthlyReportResponse = res.data;
        dispatch(setAnnualReport(response));
    } catch (e) {
        console.error(e);
    }
}

