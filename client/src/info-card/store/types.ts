import { Transaction, GetDailyReportResponse, GetMonthlyReportResponse } from "../../shared/models/entities";
import { RequestStatus } from "../../shared/models/requests";
import { InfoCardActionType } from "./constants";

export interface InfoCardState {
    transactionsToday: Transaction[];
    transactionsTodayRequestStatus: RequestStatus;
    monthlyReport: GetDailyReportResponse;
    monthlyReportRequestStatus: RequestStatus; 
    annualReport: GetMonthlyReportResponse;
}

export interface BaseInfoCardAction {
    type: InfoCardActionType;
}

export interface SetTransactionsTodayAction extends BaseInfoCardAction {
    type: InfoCardActionType.setTransactionsToday;
    transactions: Transaction[];
}

export interface SetTransactionsTodayStatusAction extends BaseInfoCardAction {
    type: InfoCardActionType.setTransactionsTodayStatus;
    status: RequestStatus;
}

export interface SetMonthlyReportAction extends BaseInfoCardAction {
    type: InfoCardActionType.setMonthlyReport;
    report: GetDailyReportResponse;
}

export interface SetMonthlyReportStatusAction extends BaseInfoCardAction {
    type: InfoCardActionType.setMonthlyReportStatus;
    status: RequestStatus;
}

export interface SetAnnualReportStatusAction extends BaseInfoCardAction {
    type: InfoCardActionType.setAnnualReport;
    report: GetMonthlyReportResponse;
}

export type InfoCardAction = SetTransactionsTodayAction |  SetTransactionsTodayStatusAction | SetMonthlyReportAction 
                            | SetMonthlyReportStatusAction | SetAnnualReportStatusAction;