export enum InfoCardActionType {
    setTransactionsToday = 'INFO_CARD/SET_TRANSACTIONS_TODAY',
    setTransactionsTodayStatus = 'INFO_CARD/SET_TRANSACTIONS_TODAY_STATUS',
    setMonthlyReport = 'INFO_CARD/SET_MONTHLY_REPORT',
    setMonthlyReportStatus = 'INFO_CARD/SET_MONTHLY_REPORT_STATUS',
    setAnnualReport = 'INFO_CARD/SET_ANNUAL_REPORT'
  }
  