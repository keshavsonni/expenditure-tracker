import { RootState } from "../../store/types";
import { createSelector  } from 'reselect';
import { generateGraphDataForDay, generateGraphDataForMonth, LineGraphData, generateGraphDataForYear } from '../models/graph';

export const transactionsTodaySelector = (state: RootState) => state.infoCard.transactionsToday;
export const transactionsTodayStatusSelector = (state: RootState) => state.infoCard.transactionsTodayRequestStatus;
export const monthlyReportSelector = (state: RootState) => state.infoCard.monthlyReport;
export const monthlyReportStatusSelector = (state: RootState) => state.infoCard.monthlyReportRequestStatus;
export const annualReportSelector = (state: RootState) => state.infoCard.annualReport;

export const transactionsTodayGraphDataSelector = createSelector(transactionsTodaySelector, (transactions): LineGraphData => {
    return generateGraphDataForDay(transactions);
});

export const monthlyReportGraphDataSelector = createSelector(monthlyReportSelector, (monthlyReport): LineGraphData => {
    return generateGraphDataForMonth(monthlyReport);
});

export const annualReportGraphDataSelector = createSelector(annualReportSelector, (annualReport): LineGraphData => {
    return generateGraphDataForYear(annualReport);
});