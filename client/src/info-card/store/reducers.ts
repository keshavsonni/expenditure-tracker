import { InfoCardState, InfoCardAction } from "./types";
import { InfoCardActionType } from "./constants";

export const initialState: InfoCardState = {
    monthlyReport: {},
    monthlyReportRequestStatus: 'none',
    transactionsToday: [],
    transactionsTodayRequestStatus: 'none',
    annualReport: {}
}

const reducer = (state: InfoCardState = initialState, action: InfoCardAction): InfoCardState => {
    switch(action.type) {
      case InfoCardActionType.setMonthlyReport:
        return {
          ...state,
          monthlyReport: action.report
        };
      case InfoCardActionType.setMonthlyReportStatus:
        return {
          ...state,
          monthlyReportRequestStatus: action.status
        }
      case InfoCardActionType.setTransactionsToday:
        return {
          ...state,
          transactionsToday: action.transactions
        }
      case InfoCardActionType.setTransactionsTodayStatus:
        return {
          ...state,
          transactionsTodayRequestStatus: action.status
        }
      case InfoCardActionType.setAnnualReport:
        return {
          ...state,
          annualReport: action.report
        }
      default:
        return state;
    }
}

export default reducer;