import React, { FunctionComponent, useState, useEffect } from 'react';
import Card from '../../shared/components/Card';
import Button, { ButtonShape } from '../../shared/components/Button';
import AddExpensesDialog from '../../table/components/AddExpenseDialog';

import '../styles/InfoCard.scss';
import TabBar, { Tab } from '../../navigation/components/TabBar';
import { isMobile } from 'react-device-detect';
import Filter from './Filter';
import SVGLineGraph from './SVGLineGraph';
import { LineGraphData } from '../models/graph';
import { useSelector, useDispatch } from 'react-redux';
import { documentSize } from '../../app/store/selectors';
import { transactionsTodayGraphDataSelector, monthlyReportGraphDataSelector, annualReportGraphDataSelector } from '../store/selectors';
import { fetchTransactionsForToday, fetchMonthlyReport, fetchAnnualReport } from '../store/action-creators';
import { transactionsList } from '../../table/store/selectors';

// Component to filter/add new transactions
const InfoCard: FunctionComponent = () => {
  // State hooks
  const [addDialogShown, showAddDialog] = useState(false);
  const [currentTab, setTab] = useState('month');
  const rootTransactions = useSelector(transactionsList);
  const dailyReport: LineGraphData = useSelector(transactionsTodayGraphDataSelector);
  const monthlyReport: LineGraphData = useSelector(monthlyReportGraphDataSelector);
  const annualReport: LineGraphData = useSelector(annualReportGraphDataSelector);
  const docSize = useSelector(documentSize);
  const dispatch = useDispatch();

  const buttonText = (): string => {
    return isMobile ? '+' : '+ Add expense';
  }

  const buttonShape = (): ButtonShape => {
    return isMobile ? 'circle' : 'rect';
  }

  const tabs: Tab[] = [
    {id: 'month', name: 'Month'},
    {id: 'year', name: 'Year'},
    {id: 'today', name: 'Today'},
    {id: 'filter', name: 'Filter'},
  ]
  
  const getGraphData = function(tabId: string): LineGraphData {
    if (tabId === 'today') {
      return dailyReport;
    }
    if (tabId === 'year') {
      return annualReport;
    }
    return monthlyReport;
  }

  const selectedTool = function(tabId: string): JSX.Element {
    const { points, xAxisLabels, yAxisLabels } = getGraphData(tabId);
    switch(tabId) {
      case 'filter':
        return <Filter />
      case 'today':
      case 'month':
      case 'year':
        return <SVGLineGraph key={tabId} interactivePoints coordinates={points} yAxisLabels={yAxisLabels} xSkipVal={isMobile ? 3 : 1} xAxisLabels={xAxisLabels} documentSize={docSize} />;
    }

    return <div></div>;
  }

  useEffect(() => {
    if (currentTab === 'today') {
      dispatch(fetchTransactionsForToday());
    } else if (currentTab === 'month') {
      dispatch(fetchMonthlyReport());
    } else if (currentTab === 'year') {
      dispatch(fetchAnnualReport());
    }
  }, [currentTab, rootTransactions, dispatch]);

  return (
    <Card grow>

        { addDialogShown ? 
          <AddExpensesDialog  onClose={() => {showAddDialog(false)}} /> 
        : ''}
        
        <div className="info-card">
            <div className="info-card__header-row">
                <div >
                  <TabBar tabs={tabs} currentTab={currentTab} onTabClick={setTab}/>
                </div>

                <div className="info-card__expense-button">
                    <Button shape={buttonShape()} onClick={ () => {showAddDialog(true)} }>
                      {buttonText()}
                    </Button>
                </div>
            </div>
            
            <div className="info-card__item-holder">
              { selectedTool(currentTab) }
            </div>
        </div>
    </Card>
  );
};

export default InfoCard;
