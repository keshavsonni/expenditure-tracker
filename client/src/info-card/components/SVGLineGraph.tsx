import React, { FunctionComponent, ReactElement, useRef, useEffect, useState, useMemo } from "react";

import '../styles/SVGLineGraph.scss';
import { remToPixels, stringifyClassMap } from "../../shared/utils/style";
import { GraphPoint } from '../models/graph';
import { size, Size, Rect, Point } from "../../shared/utils/utils";
import { cartesianCoordsToPixels } from "../../shared/utils/math";
import { lineRegion as lineRegionColor } from '../../shared/styles/Theme';
import PropTypes from 'prop-types';

const namespace = 'http://www.w3.org/2000/svg';

const POINT_SIZE_REM = 0.3;

export interface SVGLineGraphProps {
    coordinates: GraphPoint[];
    documentSize: Size;
    yAxisLabels?: string[];
    xAxisLabels?: string[];
    interactivePoints?: boolean;
    xSkipVal?: number;
}

const SVGLineGraph: FunctionComponent<SVGLineGraphProps> = function(props) {
    const [graphSize, setGraphSize] = useState(size(0,0));
    const [resizingCanvas, setResizingCanvas] = useState(true);
    const graphEl = useRef<HTMLIFrameElement>(null);
    const { documentSize, coordinates, yAxisLabels, xAxisLabels, interactivePoints, xSkipVal } = props;
    const isEmpty = coordinates.length === 0;

    const plottingArea = useMemo(() => {
        const area: Rect = {
            topLeft: {x: 40, y: 20 },
            size: { height: graphSize.height - 60, width: graphSize.width - 70 }
        };
        return area;
    }, [graphSize]);

    const points = useMemo(() => {
        const { topLeft, size } = plottingArea;
        const offset = { ...topLeft, x: topLeft.x + 2 }
        return cartesianCoordsToPixels(coordinates, size, offset);
    }, [coordinates, plottingArea]);

    const plotPoint = function(point: GraphPoint): ReactElement {
        const radius = remToPixels(POINT_SIZE_REM);
        const {x, y, id } = point;
        const key = "point-" + id;
        return (
            <circle key={key} className="svg-line-graph__circle" cx={x} cy={y} r={radius} />
        );
    }

    const plotLine = function(points: GraphPoint[]) {
        let pointsString = "";
        for (const point of points) {
            const {x, y} = point;
            pointsString = pointsString + `${x},${y} `;
        }
        pointsString = pointsString.trim();
        const dashOffset = graphSize.width * 2;

        return (
            <polyline key="line" strokeDasharray={dashOffset} strokeDashoffset={dashOffset} 
                    className="svg-line-graph__line" points={pointsString} />
        );
    }

    const plotPolygon = function(points: GraphPoint[]) {
        let pointsString = "";
        for (const point of points) {
            const {x, y} = point;
            pointsString = pointsString + `${x},${y} `;
        }
        pointsString = pointsString.trim();

        return (
            <polygon key="polygon" className="svg-line-graph__line-region" points={pointsString} />
        );
    }

    const plotLineGraph = function(points: GraphPoint[]): ReactElement[] {
        const graph: ReactElement[] = [];
        // Region below the line graph
        if (points.length > 1) {
            const bottomLeft = { ...points[0], y: graphSize.height };
            const bottomRight = { ...points[points.length - 1], y: graphSize.height };
            graph.push(plotPolygon([bottomLeft, ...points, bottomRight]));
        }

        // The line
        graph.push(plotLine(points));

        // The points
        if (interactivePoints) {
            for (const point of points) {
                graph.push(plotPoint(point));
            }
        }
        
        return graph;
    }

    const drawGridLine = function(point: Point, point2: Point, key: string, thin?: boolean): ReactElement {
        const classes = stringifyClassMap({
            'svg-line-graph__grid-line': true,
            'svg-line-graph__grid-line--thin': !!thin
        });
        return (
            <line key={key} className={classes} x1={point.x} y1={point.y} x2={point2.x} y2={point2.y} />
        );
    }

    const drawAxes = function() {
        const xStart = plottingArea.topLeft;
        const xEnd: Point = { x: xStart.x, y: xStart.y + plottingArea.size.height };
        const yStart = xEnd;
        const yEnd: Point = { x: yStart.x + plottingArea.size.width, y: yStart.y };
        
        return [
            drawGridLine(xStart, xEnd, 'x-axis'),
            drawGridLine(yStart, yEnd, 'y-axis')
        ];
    }

    const drawText = function(text: string, pixelPos: Point, key: string): ReactElement {
        const { x, y } = pixelPos;
        return (
            <text key={key} x={x} y={y} className="svg-line-graph__axis-text">{text}</text>
        );
    }

    const drawYAxisGrid = function(): ReactElement[] {
        const gridLines: ReactElement[] = [];
        
        if (!yAxisLabels) {
            return gridLines;
        }

        const numLines = yAxisLabels.length;
        const lineHeight = plottingArea.size.height / numLines;
        const { topLeft, size } = plottingArea;

        for (let i = 1; i <= numLines; i++) {
            const text = yAxisLabels[i -1];
            const y = ((numLines - i) * lineHeight) + plottingArea.topLeft.y;
            const start = {x: topLeft.x, y };
            const end = {x: topLeft.x + size.width, y };
            const textPos = {x: start.x - 30, y: y + 4 };
            if (!isEmpty) {
                gridLines.push(drawGridLine(start, end, 'grid-line-y-' + i, true));
            }
            gridLines.push(drawText(text, textPos, 'y-axis-label-' + i));
        }

        return gridLines;
    }

    const drawXAxisGrid = function(): ReactElement[] {
        const gridLines: ReactElement[] = [];
        
        if (!xAxisLabels) {
            return gridLines;
        }
        const numLines = xAxisLabels.length;
        const lineWidth = plottingArea.size.width / numLines;
        const { topLeft, size } = plottingArea;
        let textDrawInd = 0;

        for (let i = 0; i < numLines; i++) {
            const text = xAxisLabels[i];
            const x = (i * lineWidth) + plottingArea.topLeft.x;
            const y = topLeft.y + size.height;
            const start = { x, y: y+6 };
            const end = {x, y: y-6 };
            const textPos = {x: start.x - 15, y: y + 20 };
            gridLines.push(drawGridLine(start, end, 'grid-line-x-' + i));
            if (i === textDrawInd) {
                gridLines.push(drawText(text, textPos, 'x-axis-label-' + i));
                textDrawInd += 1 + (xSkipVal ?? 0);
            }  
        }

        return gridLines;
    }

    useEffect(() => {
        if(!graphEl?.current) {
            return;
        }
        setResizingCanvas(true);
        // Setting size immediately won't work as it will push the parent
        // before measuring new size
        requestAnimationFrame(() => {
            const h = graphEl?.current?.clientHeight ?? 0;
            const w = graphEl?.current?.clientWidth ?? 0;
            setGraphSize(size(w,h));
            setResizingCanvas(false);
        });
    }, [documentSize, setGraphSize, setResizingCanvas]);

    return (
        <div className="svg-line-graph__container">
            <div ref={graphEl} className="svg-line-graph__resize-listener" />
            
            { 
                graphSize.width > 0 && graphSize.height > 0 && !resizingCanvas ? 
                (
                    <svg width={graphSize.width} height={graphSize.height}  className="svg-line-graph" xmlns={namespace}>
                        <defs>
                            <linearGradient id="graphGradient" gradientTransform="rotate(90)">
                                <stop offset="0%"  stopColor={lineRegionColor} stopOpacity={1} />
                                <stop offset="80%" stopColor={lineRegionColor} stopOpacity={0} />
                            </linearGradient>
                        </defs> 
                        { drawYAxisGrid() }
                        { plotLineGraph(points) }
                        { drawAxes() }
                        { drawXAxisGrid() }
                    </svg>
                ) : ''
            }

            {
                isEmpty ?
                (
                    <div className="svg-line-graph__empty-message">No data available</div>
                ): ''
            }
        </div>
    )
}

SVGLineGraph.propTypes = {
    ...SVGLineGraph.propTypes,
    coordinates: PropTypes.array.isRequired,
    documentSize: PropTypes.shape({
        width: PropTypes.number.isRequired,
        height: PropTypes.number.isRequired
    }).isRequired
}

export default SVGLineGraph;