import React, { FunctionComponent, useState, ReactElement, useEffect } from 'react';
import Dialog, { DialogAction, DialogProps } from '../../shared/components/Dialog';
import PropTypes from 'prop-types';
import { TransactionCategory } from "../../shared/models/entities";

import '../styles/AddFilterDialog.scss';
import TextInput from '../../shared/components/TextInput';
import Dropdown, { DropdownProps } from '../../shared/components/Dropdown';
import { useSelector } from 'react-redux';
import { categoriesSelector, categoriesMapSelector } from '../../table/store/selectors';
import { useTransactionsFiltering } from '../../shared/hooks/filtering';
import { isEmpty } from '../../shared/utils/utils';
import { filterTypes as filterTypesMap, FilterType, FilterTypeId } from '../models/filtering';

export interface AddFilterDialogProps {
  onClose: () => void;
  allowedNewTypes: FilterTypeId[];
  selectedFilter?: FilterTypeId | null;
}

// A dialog to Add/Edit filters
const AddFilterDialog: FunctionComponent<AddFilterDialogProps> = (props) => {
  // Setup hooks
  const [filterType, setFilterType] = useState(filterTypesMap[props.allowedNewTypes[0]]);
  const [minVal, setMinVal] = useState(0);
  const [maxVal, setMaxVal] = useState(0);
  const [description, setDescription] = useState('');
  const [category, setCategory] = useState('');
  const categories = useSelector(categoriesSelector);
  const categoriesMap = useSelector(categoriesMapSelector);
  const filtering = useTransactionsFiltering();
  const filter = filtering[1];
  const setFilter = filtering[2];

  // Load preselected values on mount
  useEffect(() => {
    if(props.selectedFilter) {
        setFilterType(filterTypesMap[props.selectedFilter]);
        switch(props.selectedFilter) {
            case 'min-max':
                setMinVal(filter.min ?? 0);
                setMaxVal(filter.max ?? 0);
                break;
            case 'category':
                setCategory(filter.category ?? '');
                break;
            case 'description':
                setDescription(filter.description ?? '');
                break;
        }
    }
  }, [props, setFilterType, setMinVal, setMaxVal, setDescription, setCategory, filter])

  // Subset of filter types that can be set
  const filterTypes = props.selectedFilter ? [filterTypesMap[props.selectedFilter]] : props.allowedNewTypes.map(x => filterTypesMap[x]);  

  // Props for the dropdowns
  const filterTypeDropdownProps: DropdownProps<FilterType> = {
    displayKey: 'name',
    items: filterTypes,
    setValue: (val) => setFilterType(val),
    value: filterType
  };

  const categoryTypeDropdownProps: DropdownProps<TransactionCategory> = {
      displayKey: 'name',
      items: categories,
      setValue: (val) => setCategory(val.id),
      value: categoriesMap[category]
  }

  // decide dialog title
  const dialogTitle = function(): string {
    return props.selectedFilter ?  'Edit filter' : 'Add filter';
  }

  // Conditionally show input fields based on the filter type
  const filterFields = function(): ReactElement | ReactElement[] {
    switch(filterType?.id) {
        case 'min-max':
            return [ 
                <div key="min" className="add-filter-dialog__row">
                    <div className="add-filter-dialog__key">Min($)</div>
                    <div className="add-filter-dialog__value">
                        <TextInput value={minVal} type="number" onDataChange={(val) => setMinVal(val as number) }/>
                    </div>
                </div>,
                <div key="max" className="add-filter-dialog__row">
                    <div className="add-filter-dialog__key">Max($)</div>
                    <div className="add-filter-dialog__value">
                        <TextInput value={maxVal} type="number" onDataChange={(val) => setMaxVal(val as number) }/>
                    </div>
                </div> 
            ];   
        case 'description':
            return (
                <div className="add-filter-dialog__row">
                    <div className="add-filter-dialog__key">Description</div>
                    <div className="add-filter-dialog__value">
                        <TextInput value={description} onDataChange={(val) => setDescription(val as string) }/>
                    </div>
                </div>
            );
        case 'category':
            return (
                <div className="add-filter-dialog__row">
                    <div className="add-filter-dialog__key">Category</div>
                    <div className="add-filter-dialog__value">
                        <Dropdown { ...categoryTypeDropdownProps } />
                    </div>
                </div>
            );
        default:
            return (
                <div>Error: Unknown filter type</div>
            );
    }
  }
  
  // Determines whether the filter can be applied
  const validateDialogState = function(): boolean {
      switch(filterType?.id) {
          case 'min-max':
            if((isEmpty(maxVal) && isEmpty(minVal)) || 
                (!isEmpty(minVal) && minVal < 0) || 
                (!isEmpty(maxVal) && maxVal < 0) || 
                (!isEmpty(maxVal) && !isEmpty(minVal) && minVal >= maxVal)) {
                return false;
            }
            return true;
          case 'category':
              return !isEmpty(category);
          case 'description':
              return !isEmpty(description);
          default:
              return false;
      }
  }

  // Commit changes to the store
  const applyFilter = function() {
    let min = undefined;
    let max = undefined;
    switch(filterType?.id) {
        case 'min-max':
            min = !isEmpty(minVal) ? minVal : undefined;
            max = !isEmpty(maxVal) ? maxVal : undefined;
            setFilter({ ...filter, min, max });
            break;
        case 'description':
            setFilter({ ...filter, description });
            break;
        case 'category':
            setFilter({ ...filter, category });
            break; 
    }

    props.onClose();
  }

  // Clear the selected filter
  const clearFilter = function() {
      if(!props.selectedFilter) {
          return;
      }
      switch(props.selectedFilter) {
        case 'min-max':
            setFilter({ ...filter, min: undefined, max: undefined });
            break;
        case 'description':
            setFilter({ ...filter, description: undefined });
            break;
        case 'category':
            setFilter({ ...filter, category: undefined });
            break; 
      }

      props.onClose();
  }

  const dialogProps = function(): DialogProps {
    const title = dialogTitle();
    
    const cancel: DialogAction = {
        action: () => { props.onClose() },
        text: 'Cancel'
    };

    const confirm: DialogAction = {
        action: applyFilter,
        text: 'Apply',
        disabled: !validateDialogState()
    };

    const deleteAction: DialogAction | undefined = props.selectedFilter ? {
        action: clearFilter,
        text: ''
    } : undefined;

    return { title, cancel, confirm, delete: deleteAction };
  }

  return (
    <Dialog {...dialogProps()} >
        <div className="add-filter-dialog__row">
            <div className="add-filter-dialog__key">Filter type</div>
            <div className="add-filter-dialog__value">
                <Dropdown { ...filterTypeDropdownProps } />
            </div>
        </div>

        { filterFields() }
    </Dialog>
  );
};

AddFilterDialog.propTypes = {
  ...AddFilterDialog.propTypes,
  onClose: PropTypes.func.isRequired,
  allowedNewTypes: PropTypes.array.isRequired,
  selectedFilter: PropTypes.oneOf<FilterTypeId>(['min-max', 'category', 'description'])
}

export default AddFilterDialog;
