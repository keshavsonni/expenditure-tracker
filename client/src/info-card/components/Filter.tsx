import React, { FunctionComponent, ReactElement, useState } from "react";

import '../styles/Filter.scss';
import Button from "../../shared/components/Button";
import { useTransactionsFiltering } from "../../shared/hooks/filtering";
import { TransactionsFilter } from "../../shared/models/entities";
import { stringifyClassMap } from "../../shared/utils/style";
import { useSelector } from "react-redux";
import { categoriesMapSelector } from "../../table/store/selectors";
import AddFilterDialog from '../components/AddFilterDialog';
import moment from 'moment';
import { FilterTypeId } from "../models/filtering";
import { isEmpty } from "../../shared/utils/utils";

interface FilterButtonOptions {
    text: string;
    key: string;
    filterType: FilterTypeId | null;
    clip?: boolean;   
}

const Filter: FunctionComponent = () => {
    const filtering = useTransactionsFiltering();
    const filter = filtering[1];
    const categories = useSelector(categoriesMapSelector);
    const [editingFilter, setEditingFilter] = useState(false);
    const [selectedFilterType, setSelectedFilterType] = useState(null as FilterTypeId | null);

    const constructFilterButton = function(options: FilterButtonOptions): ReactElement {
        const textClasses = stringifyClassMap({
            'filter__button-text': true,
            'filter__button-text--clip': !!options.clip,           
        });

        return (
            <Button key={options.key } shape='capsule' onClick={() => {  
                setSelectedFilterType(options.filterType); 
                setEditingFilter(true); 
            } }>
                <span className={textClasses}>{options.text}</span>
            </Button>
        );
    }

    const allowedNewFilterTypes = function(): FilterTypeId[] {
        const types: FilterTypeId[] = [];

        if(isEmpty(filter.min) && isEmpty(filter.max)) {
            types.push('min-max');
        }
        if (isEmpty(filter.category)) {
            types.push('category');
        }
        if(isEmpty(filter.description)) {
            types.push('description');
        }

        return types;
    }

    const filterToButtons = function(filter: TransactionsFilter): ReactElement[] {
        const buttons: ReactElement[] = [];

        // Min max filters
        if(filter.min != null && filter.max != null) {
            buttons.push(constructFilterButton({ key:"min-max", text: `$${filter.min} to $${filter.max}`, filterType: 'min-max' } ));
        } else if (filter.min != null) {
            buttons.push(constructFilterButton({ key:"min-max", text: `Min $${filter.min}`, filterType: 'min-max'}));
        } else if (filter.max != null) {
            buttons.push(constructFilterButton({ key:"min-max", text: `Max $${filter.max}`, filterType: 'min-max'}));
        }

        // Category
        if(filter.category != null) {
            if(categories != null) {
                const category = categories[filter.category];
                if (category && category.name) {
                    buttons.push(constructFilterButton({ key:"category", text: category.name, filterType: 'category'}));
                }
            }
        }

        // Dates
        const from = filter.from ? moment(filter.from).format('DD MMM') : null;
        const to = filter.to ? moment(filter.to).format('DD MMM') : null;

        if (from && to) {
            buttons.push(constructFilterButton({ key:"dates", text: `${from} to ${to}`, filterType: null } ));
        } else if (from) {
            buttons.push(constructFilterButton({ key:"dates", text: `After ${from}`, filterType: null } ));
        } else if (to) {
            buttons.push(constructFilterButton({ key:"dates", text: `Before ${to}`, filterType: null } ));
        }

        // Description
        if(filter.description != null) {
            buttons.push(constructFilterButton({ key:"description", text: filter.description, clip: true, filterType: 'description'}));
        }

        if(allowedNewFilterTypes().length > 0) {
            buttons.push(constructFilterButton({ key:"add", text: '+ Add filter', filterType: null } ))
        }

        return buttons;
    }

    return (
        <div className='filter'>
            { editingFilter ? 
                <AddFilterDialog onClose={() => { 
                    setEditingFilter(false); 
                    setSelectedFilterType(null); 
                }} allowedNewTypes={allowedNewFilterTypes()} selectedFilter={selectedFilterType} /> : '' }

            <div className="filter__buttons-holder">
                { filterToButtons(filter) }
            </div>
        </div>
    );
}

export default Filter;