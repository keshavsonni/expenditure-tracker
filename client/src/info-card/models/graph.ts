
import { Point } from "../../shared/utils/utils";
import { shortenedNumString } from '../../shared/utils/number-format';
import moment from "moment";
import { Transaction, GetDailyReportResponse, GetMonthlyReportResponse } from "../../shared/models/entities";
import max from 'lodash/maxBy';
import sort from "lodash/sortBy";

export interface GraphPoint extends Point {
    id: string;
}

export interface LineGraphData {
    points: GraphPoint[];
    xAxisLabels: string[];
    yAxisLabels: string[];
}

export type GraphTimeRange = 'day' | 'month' | 'year';

export const getUpperLimit = function(maxNum: number) {
    const ind = Math.trunc(maxNum).toString().length - 1;
    const base10 = 10 ** ind;
    return Math.ceil(maxNum / base10) * base10;
}

export const generateYAxisLabels = function(maxNum: number): string[] {
    const baseVals = [0.25, 0.5, 0.75, 1];
    const limit = getUpperLimit(maxNum);
    const rangeVals = baseVals.map(x => x * limit);
    return rangeVals.map(x => shortenedNumString(x));
}

const xLabelsForDay = function(): string[] {
    const labels: string[] = [];
    for (let i = 0; i < 24; i++) {
        labels.push(`${i}:00`.padStart(5,'0'));
    }

    return labels;
}

const xLabelsForMonth = function(timestamp: number): string[] {
    const time = moment(timestamp).utc();
    const startOfMonth = time.startOf('month').get('date');
    const endOfMonth = time.endOf('month').get('date');
    const month = time.get('month') + 1;

    const labels: string[] = [];
    for (let i = startOfMonth; i<= endOfMonth; i++) {
        labels.push(`${i}`.padStart(2,'0') + '/' + `${month}`.padStart(2,'0'));
    }
    return labels;
}

export const generateXAxisLabels = function(timeRange: GraphTimeRange, timestamp: number) {
    switch (timeRange) {
        case 'day':
            return xLabelsForDay();
        case 'month':
            return xLabelsForMonth(timestamp);
        case 'year':
            return moment.monthsShort();
    }
}

export const generateGraphDataForDay = function(transactions: Transaction[]): LineGraphData {
    const xAxisLabels = generateXAxisLabels('day', 1);

    const maxVal = max(transactions, x => x.value)?.value ?? 10;
    const yAxisLabels = generateYAxisLabels(maxVal);
    const upperLimit = getUpperLimit(maxVal);

    const millisInDay = 86400 * 1000;

    const xCoords = transactions.map(t => {
        const timeOfDay = t.createdAt % millisInDay;
        return timeOfDay / millisInDay;
    });

    const yCoords = transactions.map(t => t.value / upperLimit);

    let points = transactions.map((val, index) => {
        const graphPoint: GraphPoint = {
            id: val.id,
            x: xCoords[index],
            y: yCoords[index]
        };
        return graphPoint;
    });
    
    points = sort(points, p => p.x);

    return { points, xAxisLabels, yAxisLabels };
}

export const generateGraphDataForMonth = function(res: GetDailyReportResponse): LineGraphData {
    const timestamps = Object.keys(res).map(x => parseInt(x));
    const totals = Object.values(res);

    if (timestamps.length < 1) {
        return {
            points: [],
            xAxisLabels: [],
            yAxisLabels: []
        }
    }

    const xAxisLabels = generateXAxisLabels('month', timestamps[0]);
    const maxTotal = max(totals) ?? 1;
    const yAxisLabels = generateYAxisLabels(maxTotal);
    const upperLimit = getUpperLimit(maxTotal);

    const minTimeStamp = moment(timestamps[0]).utc().startOf('month').startOf('day').valueOf();
    const maxTimeStamp = moment(timestamps[0]).utc().endOf('month').endOf('day').valueOf();
    const xCoords = timestamps.map(t => (t - minTimeStamp) / (maxTimeStamp - minTimeStamp));
    const yCoords = totals.map(t => t / upperLimit);

    let points = totals.map((val, index) => {
        const graphPoint: GraphPoint = {
            id: 'graph-point-' + index,
            x: xCoords[index],
            y: yCoords[index]
        };
        return graphPoint;
    });

    points = sort(points, p => p.x);

    return { points, xAxisLabels, yAxisLabels };
}

export const generateGraphDataForYear = function(res: GetMonthlyReportResponse): LineGraphData {
    const months = Object.keys(res).map(x => parseInt(x));
    const totals = Object.values(res);

    const xAxisLabels = generateXAxisLabels('year', 0);
    const maxTotal = max(totals) ?? 1;
    const upperLimit = getUpperLimit(maxTotal);
    const yAxisLabels = generateYAxisLabels(upperLimit);

    if (months.length < 1) {
        return {
            points: [],
            xAxisLabels: xAxisLabels,
            yAxisLabels: yAxisLabels
        }
    }

    const MONTHS_IN_YEAR = 12;

    const xCoords = months.map(m => m / MONTHS_IN_YEAR);
    const yCoords = totals.map(t => t / upperLimit);

    let points = totals.map((val, index) => {
        const graphPoint: GraphPoint = {
            id: 'graph-point-' + index,
            x: xCoords[index],
            y: yCoords[index]
        };
        return graphPoint;
    });

    points = sort(points, p => p.x);

    return { points, xAxisLabels, yAxisLabels };
}