import { generateYAxisLabels, getUpperLimit } from './graph';
import { MILLION } from '../../shared/utils/number-format';

describe('Graph data generation tests', () => {

    describe('Graph upper limit tests', () => {
        it('Generates reasonable upper limit', () => {
            const limit = getUpperLimit(4800);
            expect(limit).toBe(5000);
        });

        it('Handles values less than one', () => {
            const limit = getUpperLimit(0.2);
            expect(limit).toBe(1);
        });
    });

    describe('y-Axis label generation tests', () => {
        it('generates y-Axis labels in the ones range', () => {
            const maxVal = 0.9;
            const labels = generateYAxisLabels(maxVal);
            const expected = ['0.25', '0.50', '0.75', '1'];
            expect(labels).toEqual(expected);
        });
    
        it('generates y-Axis labels in the 10s range', () => {
            const maxVal = 75;
            const labels = generateYAxisLabels(maxVal);
            const expected = ['20', '40', '60', '80'];
            expect(labels).toEqual(expected);
        });
    
        it('generates y-Axis labels in the 100s range', () => {
            const maxVal = 400;
            const labels = generateYAxisLabels(maxVal);
            const expected = ['100', '200', '300', '400'];
            expect(labels).toEqual(expected);
        });
    
        it('generates y-Axis labels in the 1k range', () => {
            const maxVal = 7800;
            const labels = generateYAxisLabels(maxVal);
            const expected = ['2k', '4k', '6k', '8k'];
            expect(labels).toEqual(expected);
        });
    
        it('generates y-Axis labels in the 100k range', () => {
            const maxVal = 45000;
            const labels = generateYAxisLabels(maxVal);
            const expected = ['12.5k', '25k', '37.5k', '50k'];
            expect(labels).toEqual(expected);
        });
    
        it('generates y-Axis labels in the 1M range', () => {
            const maxVal = 3.8 * MILLION;
            const labels = generateYAxisLabels(maxVal);
            const expected = ['1M', '2M', '3M', '4M'];
            expect(labels).toEqual(expected);
        });
    });
})