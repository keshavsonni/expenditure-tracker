export type FilterTypeId = 'min-max' | 'category' | 'description';

export interface FilterType {
    id: FilterTypeId;
    name: string;
}

export const filterTypes: Record<FilterTypeId, FilterType> = {
    'min-max' : { id: 'min-max', name: 'Value' },
    'category' : { id: 'category', name: 'Category' },
    'description' : { id: 'description', name: 'Description' }
};