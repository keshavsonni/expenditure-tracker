import moment from 'moment';
import { isMobile } from 'react-device-detect';
import { Transaction, TransactionCategory } from '../../shared/models/entities';
import { ColumnDescription } from '../components/Table';

export const transactionsColumnDescription = (categoriesMap: Record<string, TransactionCategory>): ColumnDescription<Transaction>[] => {
    return [
        {
          objectKey: 'value',
          headerText: 'Value ($)',
          widthWeighting: 0.3,
          dataType: 'numeric',
          selector: (transaction: Transaction) => {
            return transaction.value ? transaction.value.toFixed(2) : '0.00'
          },
        },
        {
          objectKey: 'description',
          headerText: 'Description',
          widthWeighting: isMobile ? 0.4 : 0.3,
          dataType: 'string'
        },
        {
          objectKey: 'category',
          headerText: 'Category',
          widthWeighting: 0.3,
          dataType: 'string',
          selector: (transaction: Transaction) => {
            return categoriesMap[transaction.category]?.name || '';
          },
          hidden: isMobile
        },
        {
          objectKey: 'createdAt',
          headerText: 'Date',
          widthWeighting: isMobile ? 0.3 : 0.2,
          dataType: 'string',
          // Table runs this function to render the value
          selector: (transaction: Transaction) => {
            return moment.utc(transaction.createdAt).format('DD MMM YYYY');
          }
        },
    ];
}