export enum TableActionType {
  setTransactions = 'TABLE/SET_TRANSACTIONS',
  setTransactionsRequestStatus = 'TABLE/SET_TRANSACTIONS/REQUEST_STATUS',
  setTransactionsCategories = 'TABLE/SET_TRANSACTIONS_CATEGORIES',
  setTransactionsCategoriesStatus = 'TABLE/SET_TRANSACTIONS_CATEGORIES_STATUS',
  setTotalPages = 'TABLE/SET_TOTAL_PAGES',
  setTransactionsFilter = 'TABLE/SET_TRANSACTIONS_FILTER'
}
