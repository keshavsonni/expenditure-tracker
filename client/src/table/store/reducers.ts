import { TableActionType } from './constants';
import { arrayToMap } from '../../shared/utils/utils';
import { TableState, TableAction } from './types';

// Initial state and type
export const initialState: TableState = {
  transactions: {},
  transactionsRequestStatus: 'none',
  transactionCategories: [],
  transactionCategoriesRequestStatus: 'none',
  totalPages: 0,
  transactionsFilter: {}
};

const reducer = (state: TableState = initialState, action: TableAction): TableState => {
  switch(action.type) {
    case TableActionType.setTransactions:
      return {
        ...state,
        transactions : arrayToMap(action.transactions)
      }
      
    case TableActionType.setTransactionsRequestStatus:
      return {
        ...state,
        transactionsRequestStatus: action.status
      }

    case TableActionType.setTransactionsCategories:
      return {
        ...state,
        transactionCategories: action.categories
      }
    
    case TableActionType.setTransactionsCategoriesStatus:
      return {
        ...state,
        transactionCategoriesRequestStatus: action.status
      }
    
    case TableActionType.setTransactionsFilter:
      return {
        ...state,
        transactionsFilter: action.filter
      }

    case TableActionType.setTotalPages:
      return {
        ...state,
        totalPages: action.value
      }

    default:
      return state;
  }
};

export default reducer;
