import { Transaction, GetTransactionsResponse, TransactionCategory, 
    GetTransactionsCategoriesResponse, TransactionsFilter } from "../../shared/models/entities";
import { TableAction } from "./types";
import { TableActionType } from "./constants";
import { RequestStatus } from "../../shared/models/requests";
import { AppThunk } from "../../store/types";
import axios from "axios";
import config from "../../config";
import { constructGetQuery } from "../../shared/utils/utils";

export const setTransactions = function(transactions: Transaction[]): TableAction {
    return {
        type: TableActionType.setTransactions,
        transactions: transactions
    }
}

export const setTransactionsRequestStatus = function(status: RequestStatus): TableAction {
    return {
        type: TableActionType.setTransactionsRequestStatus,
        status: status
    }
}

export const setTransactionsCategories = function(categories: TransactionCategory[]): TableAction {
    return {
        type: TableActionType.setTransactionsCategories,
        categories
    }
}

export const setTransactionsCategoriesStatus = function(status: RequestStatus): TableAction {
    return {
        type: TableActionType.setTransactionsCategoriesStatus,
        status: status
    }
}

export const setTransactionsFilter = function(filter: TransactionsFilter): TableAction {
    return {
        type: TableActionType.setTransactionsFilter,
        filter: filter
    }
}

export const setTotalPages = function(value: number): TableAction {
    return {
        type: TableActionType.setTotalPages,
        value
    }
}

export const fetchTransactions = (filter?: TransactionsFilter): AppThunk => async (dispatch, getState) => {
    dispatch(setTransactionsRequestStatus('loading'));

    const state = getState();
    const storedFilter = state.table.transactionsFilter ?? {};
    const filterString = filter ? constructGetQuery(filter) : constructGetQuery(storedFilter); 
    try {
        // Currently locked down to 50 results. Will be customized with filters
        const res = await axios.get(`${config.apiUrl}/transactions${filterString}`, {
            withCredentials: true
        });
        dispatch(setTransactionsRequestStatus('success'));

        const response: GetTransactionsResponse = res.data;
        dispatch(setTransactions(response.result));
        dispatch(setTotalPages(response.pages));
    } catch (e) {
        console.error(e);
        dispatch(setTransactionsRequestStatus('failed'));
    }
}

export const fetchTransactionCategories = (): AppThunk => async dispatch => {
    dispatch(setTransactionsCategoriesStatus('loading'));
    try {
        const res = await axios.get(`${config.apiUrl}/categories`, {
            withCredentials: true
        });
        dispatch(setTransactionsCategoriesStatus('success'));

        const response: GetTransactionsCategoriesResponse = res.data;
        dispatch(setTransactionsCategories(response));
    } catch (e) {
        console.error(e);
        dispatch(setTransactionsCategoriesStatus('failed'));
    }
}

export const createTransaction = (transaction: Partial<Transaction>): AppThunk => async dispatch => {
    try {
        await axios.post(`${config.apiUrl}/transactions`, transaction, {
            withCredentials: true
        });
        dispatch(fetchTransactions());
    } catch(e) {
        console.error(e);
    }
}

export const updateTransaction = (transaction: Partial<Transaction>): AppThunk => async dispatch => {
    try {
        await axios.put(`${config.apiUrl}/transactions/${transaction.id}`, transaction, {
            withCredentials: true
        });
        dispatch(fetchTransactions());
    } catch(e) {
        console.error(e);
    }
}