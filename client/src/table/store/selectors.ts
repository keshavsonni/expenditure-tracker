import { RootState } from "../../store/types";
import { createSelector  } from 'reselect';
import { mapToArray, arrayToMap } from "../../shared/utils/utils";
import { TransactionCategory } from "../../shared/models/entities";

// STATIC SELECTORS
export const transactionsSelector = (state: RootState) => state.table.transactions;
export const categoriesSelector = (state: RootState) => state.table.transactionCategories;
export const categoriesStatusSelector = (state: RootState) => state.table.transactionCategoriesRequestStatus;
export const totalPagesSelector = (state: RootState) => state.table.totalPages;
export const transactionsFilterSelector = (state: RootState) => state.table.transactionsFilter;


// DYNAMIC SELECTORS

// Send list in sorted order.
// Using selectors here makes it more effecient as they only get updated on data change
export const transactionsList = createSelector(
    transactionsSelector,
    (transactions) => {
        const list = mapToArray(transactions);
        return list.sort((a, b): number => {
            if(a.createdAt === b.createdAt) {
                return 0;
            }
            if(b.createdAt > a.createdAt) {
                return 1;
            } else {
                return -1;
            }
        })
    }
);

// Helpful to store a map as well for speedy lookups
export const categoriesMapSelector = createSelector(
    categoriesSelector,
    (categories) => {
        return arrayToMap<TransactionCategory>(categories)
    }
);