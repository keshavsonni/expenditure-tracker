import { Transaction, TransactionCategory, TransactionsFilter } from "../../shared/models/entities";
import { TableActionType } from "./constants";
import { RequestStatus } from "../../shared/models/requests";

export interface TableState {
    transactions: Record<string, Transaction>;
    transactionsRequestStatus: RequestStatus;
    transactionCategories: TransactionCategory[];
    transactionCategoriesRequestStatus: RequestStatus;
    transactionsFilter: TransactionsFilter;
    totalPages: number;
}

export interface BaseTableAction {
    type: TableActionType;
}

export interface SetTransactionsAction extends BaseTableAction {
    type: TableActionType.setTransactions;
    transactions: Transaction[];
}

export interface SetTransactionsRequestStatusAction extends BaseTableAction {
    type: TableActionType.setTransactionsRequestStatus;
    status: RequestStatus;
}

export interface SetTransactionCategoriesAction extends BaseTableAction {
    type: TableActionType.setTransactionsCategories;
    categories: TransactionCategory[];
}

export interface SetTransactionCategoriesStatusAction extends BaseTableAction {
    type: TableActionType.setTransactionsCategoriesStatus;
    status: RequestStatus;
}

export interface SetTotalPagesAction extends BaseTableAction {
    type: TableActionType.setTotalPages;
    value: number;
}

export interface SetTransactionsFilter extends BaseTableAction {
    type: TableActionType.setTransactionsFilter;
    filter: TransactionsFilter;
}

export type TableAction = SetTransactionsAction | SetTransactionsRequestStatusAction | 
        SetTransactionCategoriesAction | SetTransactionCategoriesStatusAction |
        SetTotalPagesAction | SetTransactionsFilter;