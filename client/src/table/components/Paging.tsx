import React, { FunctionComponent, useState } from "react";
import PropTypes from 'prop-types';

import '../styles/Paging.scss';
import TextInput from "../../shared/components/TextInput";
import { stringifyClassMap } from "../../shared/utils/style";
import { clip } from "../../shared/utils/utils";

export interface PagingProps {
    first: number;
    last: number;
    current: number;
    onNext?: () => void;
    onPrevious?: () => void;
    onSet?: (val: number) => void;
}

const Paging: FunctionComponent<PagingProps> = (props) => { 
    const [localCurrent, setLocalCurrent] = useState(undefined as number | undefined)

    const canGoPrevious = function(): boolean {
        return props.current > props.first;
    }

    const canGoNext = function(): boolean {
        return props.current < props.last;
    }

    const containsMultiplePages = function(): boolean {
        return props.last - props.first > 0;
    }

    const previousClasses = function(): string {
        return stringifyClassMap({
            'paging__item': true,
            'paging__previous-link': true,
            'disabled': !canGoPrevious()
        });
    }

    const nextClasses = function(): string {
        return stringifyClassMap({
            'paging__item': true,
            'paging__next-link': true,
            'disabled': !canGoNext()
        });
    }
    
    const nextClicked = function() {
        if(props.onNext)
            props.onNext();
    }

    const previousClicked = function() {
        if(props.onPrevious)
            props.onPrevious();
    }

    const pageNumberSet = () => {
        if(localCurrent == null)
            return;
        // Clip invalid values into range
        const val = clip(localCurrent, props.first, props.last);
        
        if(props.onSet) {
            props.onSet(val);
        }
        setLocalCurrent(undefined);
    }

    return (
        <div className="paging">
            <div className={previousClasses()} onClick={previousClicked}>&lt;&lt; Previous</div>
            
            <div className="paging__item paging__page-indicator">
                {
                    containsMultiplePages() ?
                    <div className="paging__input-holder">     
                        <TextInput value={localCurrent ?? props.current} 
                                type="number" onBlur={pageNumberSet}
                                onDataChange={(val) => setLocalCurrent(val as number)} /> 
                    </div> :
                    <div>{ props.current }</div>
                }
                
                <div className="paging__total-pages-indicator">of {props.last}</div>
            </div>

            <div className={nextClasses()} onClick={nextClicked}>Next &gt;&gt;</div>
        </div>
    );
}

Paging.propTypes = {
    ...Paging.propTypes,
    current: PropTypes.number.isRequired,
    first: PropTypes.number.isRequired,
    last: PropTypes.number.isRequired,
    onNext: PropTypes.func,
    onPrevious: PropTypes.func,
    onSet: PropTypes.func
}

export default Paging;