import React, { FunctionComponent, useState } from 'react';
import Table, { TableDescription } from './Table';
import Card from '../../shared/components/Card';
import { Transaction } from '../../shared/models/entities';
import { useSelector } from 'react-redux';
import { transactionsList, categoriesMapSelector } from '../store/selectors';

import '../styles/TableHolder.scss';
import AddExpensesDialog from './AddExpenseDialog';
import { useTransactionsFiltering } from '../../shared/hooks/filtering';
import Paging from './Paging';
import { transactionsColumnDescription } from '../models/transactions-table';
import { documentSize } from '../../app/store/selectors';

// A wrapper around the table to make it more specific to transactions
const TableHolder: FunctionComponent = () => {
  
  // Hooks
  const transactions = useSelector(transactionsList);
  const categoriesMap = useSelector(categoriesMapSelector);
  const docSize = useSelector(documentSize);
  const [selectedRow, selectRow] = useState('');
  const [updateDialogShown, showUpdateDialog] = useState(false);
  const [totalPages, filter, setFilter] = useTransactionsFiltering();

  // Data change handlers
  const onRowSelect = (id?: string) => {
    id ? selectRow(id) : selectRow('');
    if(id != null) {
      showUpdateDialog(true);
    }
  }

  const onRowCapacityChange = (rows: number) => {
    const results = Math.max(rows, 1);
    setFilter({ page: 1, results })
  }

  // Paging action events
  const nextPage = () => {
    const page = filter.page ? filter.page + 1 : 1;
    setFilter({ ...filter, page })
  }

  const previousPage = () => {
    const page = filter.page ? filter.page - 1 : 1;
    setFilter({ ...filter, page })
  }

  const setPage = (page: number) => {
    setFilter({ ...filter, page })
  }

  const tableDescription = (): TableDescription<Transaction> => {
    return {
      // Describe which columns to display and how to render them
      columns: transactionsColumnDescription(categoriesMap),

      objects: transactions
    };
  };

  return (
    <Card grow>
      { 
        updateDialogShown ? 
        ( <AddExpensesDialog onClose={() => { showUpdateDialog(false) }} selectedId={selectedRow} /> ) : ''
      }

      <div className="table-holder">
        <div className="table-holder__body">
          <Table tableDescription={tableDescription()} selectedRowId={selectedRow} onRowSelect={onRowSelect} 
                onRowCapacityChange={onRowCapacityChange} documentSize={docSize} />
        </div>
        <div className="table-holder__footer">
          <Paging first={1} last={totalPages} 
                  current={filter.page ?? 1} 
                  onNext={nextPage} onPrevious={previousPage}
                  onSet={setPage} />
        </div>
      </div>
    </Card>
  );
};


export default TableHolder;
