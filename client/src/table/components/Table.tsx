import React, { ReactElement, useRef, useState, useEffect, CSSProperties } from 'react';
import Card from '../../shared/components/Card';
import PropTypes from 'prop-types';
import isEqual from 'lodash/isEqual';

import { ellipsis, percentWidth, stringifyClassMap, remToPixels } from '../../shared/utils/style';
import { EntityWithId } from '../../shared/models/entities';

import '../styles/Table.scss';
import { Size } from '../../shared/utils/utils';
import { usePrevious } from '../../shared/hooks/utils';

export type MixedTableData = string | number;

export type ColumnDataType = 'string' | 'numeric';

export interface ColumnDescription<T> {
  objectKey: keyof T;
  headerText: string;
  widthWeighting: number; // percentage width the column should occupy 0 - 1
  dataType?: ColumnDataType;
  selector?: (item: T) => string;
  hidden?: boolean;
}

export interface TableDescription<T> {
  objects: T[];
  columns: ColumnDescription<T>[];
}

export interface Props<T> {
  tableDescription: TableDescription<T>;
  selectedRowId?: string;
  onRowSelect?: (id?: string) => void;
  onRowCapacityChange?: (rows: number) => void;
  documentSize: Size;
}

const ROW_HEIGHT_REM = 2.5;

// A generic table that takes in column descriptions, data associated with those columns
// And renders a data table. Supports various click and select events
const Table = <T extends EntityWithId>(props: Props<T>): JSX.Element => {
  // State
  const [rowCapacity, setRowCapacity] = useState(0);
  const [rowHeight, setRowHeight] = useState(0);
  const tableEl = useRef<HTMLDivElement>(null);

  // Destructure props
  const { documentSize, onRowCapacityChange } = props;
  const previousSize = usePrevious(documentSize);
  
  // Styling for the rows in different states
  const rowClasses = function(isSelected: boolean, isLast: boolean): string {
    return stringifyClassMap({
      'table__row': true,
      'table__row--data': true,
      'table__row--selected': isSelected,
      'table__row--final': isLast
    });
  }

  // Actions to take when a row is selected
  const onRowClicked = function(item: T) {
    if(props.onRowSelect) {
      // Unselect if already selected
      if(props.selectedRowId === item.id) {
        props.onRowSelect(undefined);
      } else {
        props.onRowSelect(item.id);
      }
    }
  }
  
  // Function to generate a row from object
  const row = (item: T, isSelected: boolean, isLast: boolean): ReactElement => {
    if (!props.tableDescription?.columns) {
      return <div className="table__row table__row--data"></div>;
    }
    const cols: ReactElement[] = [];
    for (const description of props.tableDescription?.columns) {
      if(description.hidden) {
        continue;
      }
      const style: React.CSSProperties = {
        ...percentWidth(description.widthWeighting),
        ...ellipsis
      };

      const col = (
        <span key={description.objectKey as string} style={style}>
          {description.selector ? description.selector(item) : item[description.objectKey]}
        </span>
      );
      cols.push(col);
    }

    const rowStyle: CSSProperties = { height: rowHeight + 'px' };

    return (
      <div style={rowStyle} className={rowClasses(!!isSelected, isLast)} key={item.id} onClick={() => onRowClicked(item)}>
        {cols}
      </div>
    );
  };

  // Aggregation of rows in the table
  const rows = (): ReactElement[] | null => {
    if (!props.tableDescription?.objects) {
      return [];
    }
    const rows: ReactElement[] = [];
    for (const [i, item] of props.tableDescription.objects.entries()) {
      const isLast = (i === props.tableDescription.objects.length - 1) && i >= rowCapacity - 1;
      rows.push(row(item, item.id === props.selectedRowId, isLast));
    }

    if(rows.length > 0) {
      return rows;
    } else {
      return null;
    }
  };

  // Function to construct header row
  const header = (): ReactElement => {
    if (!props.tableDescription?.columns) {
      return <div className="table__row table__row--data"></div>;
    }
    const cols: ReactElement[] = [];
    for (const column of props.tableDescription?.columns) {
      if(column.hidden) {
        continue;
      }
      const style: React.CSSProperties = {
        ...percentWidth(column.widthWeighting),
        ...ellipsis
      };

      const col = (
        <span key={column.objectKey as string} style={style}>
          {column.headerText}
        </span>
      );
      cols.push(col);
    }

    return <div className="table__row table__row--header">{cols}</div>;
  };

  const emptyMessage = function(): ReactElement {
    return (<div className="table__empty-message">No data found</div>)
  }

  
  // Calculate ideal row capacity and height based on table resize
  useEffect(() => {
    if(isEqual(previousSize, documentSize)) {
      return;
    }
    const height = tableEl?.current?.offsetHeight;
    if(!height) {
      return;
    }
    let rowHeight = remToPixels(ROW_HEIGHT_REM);
    const newCapacity = Math.floor(height / rowHeight);
    rowHeight = newCapacity > 0 ? height / newCapacity : rowHeight;
    if (rowCapacity !== newCapacity && onRowCapacityChange) {
      onRowCapacityChange(newCapacity);
    }
    setRowHeight(rowHeight);
    setRowCapacity(newCapacity);
  }, [documentSize, rowCapacity, previousSize, onRowCapacityChange]);

  return (
    <Card noShadows grow>
      <div className="table">
        {header()}

        <div className="table__rows-container">
          <div ref={tableEl} className="table__resize-listener" />
          { rows() ?? emptyMessage() }
        </div>
      </div>
    </Card>
  );
};

Table.propTypes = {
  tableDescription: PropTypes.shape({
    objects: PropTypes.array.isRequired,
    columns: PropTypes.arrayOf(
      PropTypes.shape({
        objectKey: PropTypes.string.isRequired,
        headerText: PropTypes.string.isRequired,
        widthWeighting: PropTypes.number.isRequired,
        dataType: PropTypes.oneOf(['string', 'numeric'])
      })
    )
  }).isRequired,
  documentSize: PropTypes.object.isRequired
};

export default Table;
