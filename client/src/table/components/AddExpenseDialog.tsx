import React, { FunctionComponent, useState, useEffect } from 'react';
import Dialog from '../../shared/components/Dialog';
import TextInput from '../../shared/components/TextInput';
import Dropdown, { DropdownProps } from '../../shared/components/Dropdown';
import PropTypes from 'prop-types';

import '../styles/AddExpenseDialog.scss';
import { TransactionCategory, Transaction } from '../../shared/models/entities';
import { useSelector, useDispatch } from 'react-redux';
import { categoriesSelector, transactionsSelector, categoriesMapSelector } from '../store/selectors';
import { createTransaction, updateTransaction } from '../store/action-creators';

export interface AddExpenseDialogProps {
  onClose:  () => void;
  selectedId? : string;
}

// A dialog to Add/Edit expenses
const AddExpensesDialog: FunctionComponent<AddExpenseDialogProps> = (props) => {
  // Declare use hooks
  const categories = useSelector(categoriesSelector);
  const [value, setValue] = useState(0);
  const [description, setDescription] = useState('');
  const [category, setCategory] = useState(categories[0]);
  const dispatch = useDispatch();
  const transactionsLookupMap = useSelector(transactionsSelector);
  const categoryMap = useSelector(categoriesMapSelector);

  const id = props.selectedId;

  // Make a copy of the selected transaction to edit on mount
  useEffect(() => {
    if(id && transactionsLookupMap[id]) {
      const transaction = transactionsLookupMap[id];
      setValue(transaction.value);
      setDescription(transaction.description ?? '');
      if(transaction.category) {
        const category = categoryMap[transaction.category];
        if(category) {
          setCategory(category);
        }
      }
    }
  }, [transactionsLookupMap, categoryMap ,setValue, setDescription, setCategory, id])

  // Data change functions
  const onValueChange = (val: number | string | undefined) => {setValue(val as number)};
  const onDescriptionChange = (val: number | string | undefined) => {setDescription(val as string)};
  
  // Dropdown props
  const dropdownProps: DropdownProps<TransactionCategory> = {
    displayKey: 'name',
    items: categories,
    setValue: setCategory,
    value: category
  };

  // Validate the data
  const isDataValid = value > 0 && description != null && description.length > 0;

  const saveData = async function() {
    const payload: Partial<Transaction> = {
      category: category.id,
      description,
      value
    };

    if(!props.selectedId) {
      dispatch(createTransaction(payload));
    } else {
      payload.id = props.selectedId;
      dispatch(updateTransaction(payload));
    }

    props.onClose(); 
  }

  // decide dialog title
  const dialogTitle = function(): string {
    return props.selectedId ? 'Update expense' : 'Add expense';
  }

  return (
    <Dialog title={dialogTitle()} cancel={{
      action: () => { props.onClose();  },
      text: 'Cancel'
    }} confirm={{
        action: saveData,
        text: 'Save',
        disabled: !isDataValid
    }}>
      <div className="add-expense-dialog__row">
        <div className="add-expense-dialog__key">Value ($)</div>
        <div className="add-expense-dialog__value">
          <TextInput value={value} type="number" onDataChange={onValueChange}/>
        </div>
      </div>

      <div className="add-expense-dialog__row">
        <div className="add-expense-dialog__key">Category</div>
        <div className="add-expense-dialog__value">
          <Dropdown { ...dropdownProps }  />
        </div>
      </div>

      <div className="add-expense-dialog__row">
        <div className="add-expense-dialog__key">Short description</div>
        <div className="add-expense-dialog__value">
          <TextInput value={description} type="string" onDataChange={onDescriptionChange}/>
        </div>
      </div>
    </Dialog>
  );
};

AddExpensesDialog.propTypes = {
  ...AddExpensesDialog.propTypes,
  onClose: PropTypes.func.isRequired
}

export default AddExpensesDialog;
