import { isDev } from "./shared/utils/utils";

export interface Config {
    apiUrl: string;
    guestAccount: string;
    features: {
        dataVisualisation: boolean;
    };
    social: {
        linkedIn: string;
        gmail: string;
        github: string;
    }
}

const config: Config = {
    apiUrl: isDev() ? 'http://localhost:3000/api/v1' : '/api/v1',

    guestAccount: 'guest@okadadigital.com',

    social: {
        linkedIn: 'https://www.linkedin.com/in/keshav-sonni',
        gmail: 'mailto:keshav.vm97@gmail.com',
        github: 'https://gitlab.com/keshavsonni/expenditure-tracker'
    },
    
    features : { // Ability to turn on/of UI features
        dataVisualisation: false
    }
}

export default config;