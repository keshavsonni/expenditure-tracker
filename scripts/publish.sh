# make the variables more readable
module=$1
user=$2
hostname=$3

# Validate the input parameters
if [ -z $module ] || [ -z $user ] || [ -z $hostname ] ; then
    echo ERROR expected syntax: \<module\> \<user\> \<hostname\>
    exit 1
fi

# Archive the module directory
echo Compressing files..
cd ../$module && git archive -o ../$module.zip HEAD || exit 1

echo Publishing $module to host $hostname..

# Upload the archive to the server
cd .. && scp ./$module.zip $user@$hostname:~/ExpenditureTracker/ || exit 1

# Do the publish
ssh $user@$hostname << UPDATE
cd ~/ExpenditureTracker
echo Clearing existing files..
rm -rf ./$module
mkdir $module
mv ./$module.zip ./$module
cd $module
echo Writing new files..
unzip -q $module.zip
rm $module.zip
npm install
npm run publish
exit
UPDATE

# Clean up
rm ./$module.zip