import { expressApp } from './config/express';
import { Server } from 'http';
import { config } from './config/config';

const app = expressApp();
const server = new Server(app);
server.listen(config.port);

// Start the server
console.log(`${process.env.NODE_ENV} server running on port ${config.port}`);