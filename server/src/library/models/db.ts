// Models used by the db
export type DbCollection = 'users';

export interface Transaction {
    id: string;
    createdAt: number;
    category: number;
    description?: string;
    value: number;
}

export interface TransactionCategory {
    id: number;
    name: string;
}

export interface User {
    id?: string;
    _id: string;
    name: string;
    email: string;
    password: string;
    createdAt: number;
    currency: string;
    totalTransactions: number;
    transactions: Transaction[];
}