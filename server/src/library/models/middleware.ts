import { AuthMethod, AuthUser } from "../../app/auth/models";
import { GetTransactionQuery, DeleteTransactionQuery, UpdateTransactionQuery, GetCategoriesReportQuery } from '../../app/transactions/models';
import { Request } from 'express';

export interface MiddlewareResultMap {
    authMethod: AuthMethod;
    authUser: AuthUser;
    getTransactionQuery: GetTransactionQuery;
    deleteTransactionQuery: DeleteTransactionQuery;
    updateTransactionQuery: UpdateTransactionQuery;
    getCategoriesReportQuery: GetCategoriesReportQuery;
}

// Typed functions to append objects to the req objects 
// Helps to cleanly pass results
export const setMiddlewareResult = function<T extends keyof MiddlewareResultMap>(req: Request, type: T, result: MiddlewareResultMap[T]) {
    (req as any)[type] = result;
}

export const getMiddlewareResult = function<T extends keyof MiddlewareResultMap>(req: Request, type: T) : MiddlewareResultMap[T] {
    return (req as any)[type];
}