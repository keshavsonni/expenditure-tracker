import { getDbCollection } from '../../config/db';
import { User } from '../models/db';
import { FilterQuery, InsertOneWriteOpResult, FindOneOptions, WriteOpResult, UpdateWriteOpResult, DeleteWriteOpResultObject } from 'mongodb';
import { hashPassword } from '../utils/crypto';
import { listTransactions } from '../transactions/transactionManager';

export const findUsers = function(query?: FilterQuery<User>, projection?: Partial<Record<keyof User, number>>): Promise<User[]> {
    const collection = getDbCollection('users');
    
    return collection?.find(query, {
        projection: projection
    }).toArray() as Promise<User[]>;
}

export const createUser = function(user: Omit<User, '_id'>): Promise<InsertOneWriteOpResult<User>> {
    const collection = getDbCollection('users');
    
    return new Promise(async (resolve, reject) => {
        const password = await hashPassword(user.password);
        const createdAt = new Date().getTime();
        
        const updated: Omit<User, '_id'> = {
            ...user,
            password,
            createdAt,
            transactions: [],
        };
        
        let result = null;
        try {
            result = await collection.insertOne(updated) as InsertOneWriteOpResult<User>;
        } catch(e) {
            reject(e);
        }
        
        resolve(result);
    });
}

export const updateUser = function(id: string, update: Partial<User>) : Promise<UpdateWriteOpResult> {
    const collection  = getDbCollection('users');

    return collection.updateOne({ _id:  id}, { $set: update });
}

export const deleteUser = function(id: string) : Promise<DeleteWriteOpResultObject> {
    const collection  = getDbCollection('users');
    
    return collection.deleteOne({_id: id});
}

// Adding mongo db constraints
export const setupConstraints = function(): Promise<string> {
    const collection  = getDbCollection('users');

    const uniqueConstraint: Partial<Record<keyof User, 1 | 0>> = {
        email: 1
    };
    return collection.createIndex(uniqueConstraint, { unique: true });
}

export const updateTotalTransactionsForUser = async function(userId: string): Promise<void> {
    const totalTransactions = await listTransactions(userId, {kind: 'fields'}, true) as number;
    await updateUser(userId, { totalTransactions });
    return Promise.resolve();
}