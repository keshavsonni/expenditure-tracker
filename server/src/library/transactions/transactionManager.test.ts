import { constructListTransactionQuery } from "./transactionManager";

describe('Transaction manager', () => {

    describe('List transactions query generation', () => {
        
        it('handles null queries', () => {
            expect(constructListTransactionQuery({ kind: 'fields' })).toBeNull();
        });

        it('generates from query', () => {
            const from = { $match: {
                'transactions.createdAt' : { $gte: 1 }
            }}
            const actual = constructListTransactionQuery({ kind: 'fields', from: 1})
            expect(actual).toEqual(from);
        });

        it('generates to query', () => {
            const to = { $match: {
                'transactions.createdAt' : { $lte: 1 }
            }}
            const actual = constructListTransactionQuery({ kind: 'fields', to: 1})
            expect(actual).toEqual(to);
        });
        
        it('generates from and to query', () => {
            const fromTo = { $match: {
                'transactions.createdAt' : { $gte: 1, $lte: 4 }
            }}
            const actual = constructListTransactionQuery({ kind: 'fields', from: 1, to: 4})
            expect(actual).toEqual(fromTo);
        });

        it('generates describe query', () => {
            const description = { $match: {
                'transactions.description' : { $regex: new RegExp('.*description.*', 'i') }
            }}
            const actual = constructListTransactionQuery({ kind: 'fields', description : 'description' });
            expect(actual).toEqual(description);
        });
        
        it('generates description & date combined query', () => {
            const description = { $match: {
                'transactions.description' : { $regex: new RegExp('.*description.*', 'i') },
                'transactions.createdAt' : { $gte: 1, $lte: 4 }
            }}
            const actual = constructListTransactionQuery({ kind: 'fields', description : 'description',  from: 1, to: 4 });
            expect(actual).toEqual(description);
        });

        it('generates min value query', () => {
            const min = { $match: {
                'transactions.value' : { $gte: 1 }
            }}
            const actual = constructListTransactionQuery({ kind: 'fields', min: 1})
            expect(actual).toEqual(min);
        });

        it('generates max value query', () => {
            const max = { $match: {
                'transactions.value' : { $lte: 1 }
            }}
            const actual = constructListTransactionQuery({ kind: 'fields', max: 1})
            expect(actual).toEqual(max);
        });

        it('generates min and max value query', () => {
            const maxMin = { $match: {
                'transactions.value' : { $lte: 4, $gte: 1 }
            }}
            const actual = constructListTransactionQuery({ kind: 'fields', min: 1, max: 4 })
            expect(actual).toEqual(maxMin);
        });

        it('generates description, value & date combined query', () => {
            const maxMin = { $match: {
                'transactions.value' : { $lte: 4, $gte: 1 },
                'transactions.description' : { $regex: new RegExp('.*description.*', 'i') },
                'transactions.createdAt' : { $gte: 1, $lte: 4 }
            }}
            const actual = constructListTransactionQuery({ kind: 'fields', min: 1, max: 4, description : 'description',  from: 1, to: 4 })
            expect(actual).toEqual(maxMin);
        });
        
        it('generates category query', () => {
            const cats = { $match: {
                'transactions.category' : { $eq: 1 }
            }}
            const actual = constructListTransactionQuery({ kind: 'fields', category: 1 })
            expect(actual).toEqual(cats);
        });

        it('generates description, value, date & category combined query', () => {
            const maxMin = { $match: {
                'transactions.value' : { $lte: 4, $gte: 1 },
                'transactions.description' : { $regex: new RegExp('.*description.*', 'i') },
                'transactions.createdAt' : { $gte: 1, $lte: 4 },
                'transactions.category' : { $eq: 1 }
            }}
            const actual = constructListTransactionQuery({ kind: 'fields', min: 1, max: 4, description : 'description',  from: 1, to: 4, category: 1 })
            expect(actual).toEqual(maxMin);
        });
    });
}); 