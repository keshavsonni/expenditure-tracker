import { TransactionCategory } from "../models/db";

// Will move to DB soon
export const listCategories = function(): TransactionCategory[] {
    return [
        { id: 1, name: 'Living' },
        { id: 2, name: 'Transportation' },
        { id: 3, name: 'Utilities' },
        { id: 4, name: 'Healthcare' },
        { id: 5, name: 'Entertainment' },
        { id: 6, name: 'Other' },
    ]
};

export const isValidCategoryId = function(id: number): boolean {
    const categories = listCategories();
    return categories.some(x => {
        return x.id === id;
    });
}