import { Transaction, User } from "../models/db";
import { getDbCollection } from "../../config/db";
import { uuid } from 'uuidv4';
import { GetTransactionByFieldsQuery, UpdateTransactionQuery, GetCategoriesReportQuery, CategoriesReport } from "../../app/transactions/models";
import { AggregationCursor, UpdateWriteOpResult } from "mongodb";
import { isEmpty, PaginatedResult } from "../utils";
import { applyPagination } from "../utils/db";
import { listCategories } from "./categoryManager";
import groupBy from 'lodash/groupBy';
import { Dictionary } from "lodash";
import moment from 'moment';

// Insert transaction to the db
export const createTransaction = function(userId: string, transaction: Partial<Transaction>): Promise<Transaction> {
    const collection = getDbCollection('users');

    return new Promise(async (resolve, reject) => {
        const full = {
            ...transaction,
            id: uuid(),
            createdAt: moment().utc().valueOf()
        } as Transaction;

        try {
            await collection.updateOne({_id: userId}, 
                            { $push: { transactions: full } });
            resolve(full);
        } catch (e) {
            reject(e);
        }
    })
}

// Find individual transaction by id
export const findTransaction = function(userId: string, transactionId: string): Promise<Transaction> {
    const collection = getDbCollection('users');

    return new Promise (async (resolve, reject) => {
        let users = null as Partial<User>[];
        try {
            users = await collection.aggregate([
                { $match: {_id: userId}},
                { $project: {
                    transactions: {
                        $filter: {
                            input: '$transactions',
                            as: 'item',
                            cond: {$eq: ['$$item.id', transactionId]}
                        }
                    }
                }}
            ]).toArray();
        } catch(e) {
            reject(e);
        }
        
        if(!users || users.length < 1 || !users[0].transactions || users[0].transactions.length === 0) {
            resolve(null);
            return;
        }

        resolve(users[0].transactions[0]);
    });
};

// User can supply various queries. 
export function constructListTransactionQuery(query: GetTransactionByFieldsQuery): Object {
    // Date limits
    let dateLimit: Record<string, any> = null;
    if(query.from != null || query.to != null) {
        dateLimit = {};
        if(query.from != null) {
            dateLimit['$gte'] = query.from;
        }
        if(query.to != null) {
            dateLimit['$lte'] = query.to;
        }
    }

    // Description
    let descriptionMatch: Record<string, RegExp> = null;
    if(!isEmpty(query.description)) {
        const descriptionPattern = `.*${query.description}.*`;
        const regex = new RegExp(descriptionPattern, 'i');
        descriptionMatch = { '$regex' :  regex};
    }

    // Value limit
    let valueLimit: Record<string, any> = null;
    if(query.min != null || query.max != null) {
        valueLimit = {};
        if(query.min != null) {
            valueLimit['$gte'] = query.min;
        }
        if(query.max != null) {
            valueLimit['$lte'] = query.max;
        }
    }

    // Category
    const categoryMatch: Record<string, any> = query.category != null ? { $eq: query.category } : null;

    if(dateLimit == null && descriptionMatch == null && valueLimit == null && categoryMatch == null) {
        return null;
    }

    const prefix = 'transactions.';
    const match: Record<string, any> = {};
    if(dateLimit != null) {
        match[prefix + 'createdAt'] = dateLimit;
    }
    if(descriptionMatch != null) {
        match[prefix + 'description'] = descriptionMatch;
    }
    if(valueLimit != null) {
        match[prefix + 'value'] = valueLimit;
    }
    if(categoryMatch != null) {
        match[prefix + 'category'] = categoryMatch;
    }

    return { $match: match };
}

export const listTransactions = async function(userId: string, query: GetTransactionByFieldsQuery, countOnly?: boolean): Promise<PaginatedResult<Transaction> | number> {
    const collection = getDbCollection('users');

    const pipeline = [];

    // Look for our user
    pipeline.push({ 
        $match: { _id: userId}
    });
    
    // Expand the transactions array into a stream of documents
    pipeline.push({ $unwind: '$transactions' })
    
    // Perform any filtering
    const match = constructListTransactionQuery(query);
    if(match != null) {
        pipeline.push(match);
    }

    // Sort by descending order of creation
    pipeline.push({ 
        $sort: { 'transactions.createdAt': -1}
    });
    
    // project just required fields
    pipeline.push({ $project: { 'transactions' : 1, _id: 0 } });

    let cursor: AggregationCursor = collection.aggregate(pipeline);

    const countingPipeline = [ ...pipeline, { $count: 'transactions' } ];
    const total = await collection.aggregate(countingPipeline).toArray();
    let countTotal = 0;
    if(total.length > 0) {
        countTotal = total[0].transactions ?? 0;
    }
    if(countOnly) {
        return countTotal;
    }

    // Paginated result control
    const paginationResult = await applyPagination(cursor, query, countTotal);
    cursor = paginationResult.cursor as AggregationCursor;

    const results: Record<'transactions', Transaction>[] =  await cursor.toArray();

    const transactions = results.map((result) => {
        return result.transactions;
    })

    return {
        pages: paginationResult.pages,
        result: transactions
    };
}

export const deleteTransaction = async function(userId: string, transactionId: string): Promise<UpdateWriteOpResult> {
    const collection = getDbCollection('users');
    
    return collection.updateOne(
        { _id: userId },
        { $pull: { 'transactions' : { id: transactionId } } }
    );
}

export const constructUpdateTransactionQuery = function(query: UpdateTransactionQuery): Object {
    const prefix = 'transactions.$.';
    const set: Record<string, any> = {};
    if(query.value != null) {
        set[prefix + 'value'] =  query.value;
    }
    if(query.description != null) {
        set[prefix + 'description'] =  query.description;
    }
    if(query.category != null) {
        set[prefix + 'category'] = query.category;
    }
    return set;
}

export const updateTransaction = async function(userId: string, query: UpdateTransactionQuery): Promise<UpdateWriteOpResult> {
    const collection = getDbCollection('users');
    
    return collection.updateOne({
            _id: userId, 
            transactions: { 
                $elemMatch: { id: { $eq: query.id } } 
            } 
        },
        { $set: constructUpdateTransactionQuery(query) }
    );
}

export const getCategoriesReport = async function(userId: string, query: GetCategoriesReportQuery): Promise<CategoriesReport> {
    const categories = listCategories();
    const report : CategoriesReport = {};

    // Calculate the total
    let total = 0;
    const totalQuery: GetTransactionByFieldsQuery = {
        ...query,
        kind: 'fields'
    }
    
    try {
        const totalResult = await listTransactions(userId, totalQuery) as PaginatedResult<Transaction>;
        total = sumTransactions(totalResult.result);
    } catch(e) {
        console.error(e);
        return Promise.reject('Failed to generate report');
    }

    // Return 0s if total is 0
    if(total === 0) {
        for (const category of categories) {
            report[category.id] = 0;
        }
        return report;
    }

    for (const category of categories) {
        const catQuery: GetTransactionByFieldsQuery = {
            ...query,
            kind: 'fields',
            category: category.id
        }
        try {
            const result = await listTransactions(userId, catQuery) as PaginatedResult<Transaction>;
            report[category.id] = sumTransactions(result.result) / total; // send percentages
        } catch(e) {
            console.error(e);
            return Promise.reject('Failed to generate report');
        }
    }

    return report;
}

export const sumTransactions = function(transactions: Transaction[]) {
    if(!transactions || transactions.length === 0) {
        return 0;
    }
    const aggregate = transactions.reduce((previous, current) => {
        return {
            value: previous.value + current.value
        } as Transaction;
    });

    return aggregate.value;
}


export const aggregateTransactionsByDay = function (transactions: Transaction[]): Dictionary<number> {
    const millisInADay = 86400 * 1000;
    const groupMap = groupBy(transactions, (t) => {
        return t.createdAt - (t.createdAt % millisInADay);
    })
    const result: Dictionary<number> = {}
    const keys = Object.keys(groupMap);
    for (const key of keys) {
        const group = groupMap[key];
        if (!group || group?.length < 1) {
            continue;
        }
        const sum = group.reduce((x, y) => { return {...y, value: x.value + y.value } }).value;
        result[key] = sum;
    }

    return result;
}

export const aggregateTransactionsByMonth = function(transactions: Transaction[]): Dictionary<number> {
    const taggedTransactions = transactions.map(x => {
        return { 
            month: new Date(x.createdAt).getUTCMonth(),
            value: x.value
        };
    });

    const dict: Dictionary<number> = {};
    for (const trans of taggedTransactions) {
        const monthStr = `${trans.month}`;
        let val = dict[monthStr] ?? 0;
        val += trans.value;
        dict[monthStr] = val;
    }

    return dict;
}