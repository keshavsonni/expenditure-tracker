import { Response } from 'express';

export interface FieldLimit {
    minChars?: number;
    maxChars?: number;
    minVal?: number;
    maxVal?: number;
}

const send = function(res: Response, status: number, data?: any) {
    if(!data) {
        res.sendStatus(status)
    } else {
        res.status(status).send(data);
    }
}

// A helper module to make sending http errors easier
export const httpError = {
    missingParameters: function(res: Response) {
        send(res, 400, 'One or more missing parameters.');
    },

    invalidValue: function <T> (res: Response, field: keyof T, limit?: FieldLimit) {
        let limitMsg = '';
        if(limit?.minChars != null || limit?.maxChars != null) {
            if (limit?.minChars != null) {
                limitMsg = ' Min chars: ' + limit.minChars + '.';
            }
            if (limit?.maxChars != null) {
                limitMsg = limitMsg + ' Max chars: ' + limit?.maxChars + '.';
            }
        } else if (limit?.minVal != null || limit?.maxVal != null) {
            if (limit?.minVal != null) {
                limitMsg = ' Min value: ' + limit.minVal + '.';
            }
            if (limit?.maxVal != null) {
                limitMsg = limitMsg + ' Max value: ' + limit?.maxVal + '.';
            }
        }
        let message = `Invalid value for the field: ${field}.`;
        if(limitMsg != '') {
            message = message + limitMsg;
        }
        send(res, 400, message);
    },

    badRequest: function(res: Response, message?: string) {
        send(res, 400, message);
    },

    failedOperation: function(res: Response, message?: string) {
        send(res, 500, message);
    },

    forbiddenOperation: function(res: Response, message?: string) {
        send(res, 403, message);
    },

    authFailed: function(res: Response, message?: string) {
        send(res, 401, message);
    },

    notFound: function(res: Response, message?: string) {
        send(res, 404, message);
    },

    insufficientStorage: function(res: Response, message?: string) {
        send(res, 507, message);
    }
}