import crypto from 'bcrypt';
import jwt from 'jsonwebtoken';

// Cryptographic functions for JSON web tokens and password management

export const hashPassword = function(password: string): Promise<string> {
    return crypto.hash(password, 10);
}

export const comparePasswords = function(input: string, hash: string): Promise<boolean> {
    return crypto.compare(input, hash);
}

export const generateAccessToken = function (data: string | object, durationSeconds: number, secret: string) : string{
    var token = jwt.sign(
        data, 
        secret, {
            expiresIn: durationSeconds
        }
    );
    return token
}

export const verifyAccessToken = function(token: string, secret: string) : string | object {
    try {
        var verification = jwt.verify(token,secret);
        return verification;
    } catch(error) {
        return null;
    }
}