export interface WithId {
    id?: string;
    _id: string;
}

export interface PaginatedResult<T> {
    pages: number;
    result: T[];
}
// Utility string manipulation functions
export const isValidEmail = function (emailAddress: string): boolean {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
};

export const isEmpty = function(str: string) {
    return str == null || str === '';
}

export const isDev = function(): boolean {
    return process.env.NODE_ENV === 'dev';
}
 
export const prettyId = function <T extends WithId> (item: T): T {
    const copy: T = { ...item, id: item._id };
    delete copy['_id'];
    return copy;
}

export const isNonEmptyNaN = function(val: number): boolean {
    return val != null && isNaN(val);
}