import { filterXSS } from 'xss';
import { Request, Response } from 'express';

// Deep XSS traversal to sanitize all input for XSS before it enters the app
function traverse (x: any) {
    if (isArray(x)) {
      return;
    } else if ((typeof x === 'object') && (x !== null)) {
      traverseObject(x)
    }
}
  
function traverseObject(obj: any) {
    for (let key in obj) {
        if(typeof obj[key] === 'string') {
            obj[key] = filterXSS(obj[key]);
        }
        if (obj.hasOwnProperty(key)) {
            traverse(obj[key])
        }
    }
}
  
function isArray(o: any): boolean {
    return Object.prototype.toString.call(o) === '[object Array]'
}

// WARNING: does not handle arrays yet
function sanitizeXSSForObject(obj: any) {
    traverse(obj);
}

export const xssSanitizer = function(req: Request, res: Response, next: () => void) {
    sanitizeXSSForObject(req.body);
    sanitizeXSSForObject(req.query);
    sanitizeXSSForObject(req.params);
    next();
}