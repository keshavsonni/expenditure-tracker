import { Cursor, AggregationCursor } from "mongodb";

export interface PaginationParams {
    results?: number;
    page?: number;
}

export interface PaginationResult {
    pages : number;
    cursor: Cursor | AggregationCursor;
}

// Function to accept a MongoDB result and pageinate
export const applyPagination = function(cursor: AggregationCursor | Cursor, params: PaginationParams, total: number): PaginationResult {
    let c = cursor;
    if(params.page == null && params.results != null) {
        c = c.limit(params.results);
        return {
            cursor : c,
            pages: Math.ceil(total / params.results)
        };
    }

    if(params.page != null && params.results != null) {
        let recordsToSkip = (params.page - 1) * params.results;

        if(total <= recordsToSkip) {
            recordsToSkip = Math.floor(total / params.results) * params.results;
        }

        c = cursor.skip(recordsToSkip).limit(params.results);
        return {
            cursor: c,
            pages: Math.ceil(total / params.results)
        };
    }
    
    return {
        cursor: c,
        pages: 1
    }
}