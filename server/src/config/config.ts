import fs from 'fs';

interface Config {
    dbName: string;
    port: number;
    corsUrl: string;
    apiPath: string;
    jwtSecret: string;
    jwtLifeSeconds: number;
    maxTransactionsPerUser: number;
};

const secretPath = '/var/jwt/key.txt';

const jwtSecret = fs.readFileSync(secretPath).toString();

export const config : Config = {
    dbName : 'finances',
    port: 3000,
    corsUrl: 'http://localhost:5000', // This is for ignoring cors on dev
    apiPath: '/api/v1/',
    jwtSecret,
    jwtLifeSeconds: 14400,
    maxTransactionsPerUser: 10000
};