import express, { Application, Request, Response } from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';

import { config } from './config';
import { setupRoutes as setupUsersRoutes } from '../app/users/usersRouter';
import { setupRoutes as setupAuthRoutes } from '../app/auth/authRouter';
import { setupRoutes as setupTransactionRoutes } from '../app/transactions/transactionsRouter';

import { setupDb } from './db';
import { httpError } from '../library/utils/http';
import { xssSanitizer } from '../library/utils/xss';

export const expressApp = function(): Application {
    const app: Application = express();

    // Connect to MongoDB
    setupDb();

    // Add bodyparser middleware
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    // Body parser converts requests to JSON
    app.use(bodyParser.json());

    // Check for invalid json
    app.use(function (error: any, req: Request, res: Response, next: () => void) {
        if (error instanceof SyntaxError) {
            return httpError.badRequest(res, 'Error in JSON syntax');
        } else {
            next();
        }
    });

    // Use cookie parser
    app.use(cookieParser());

    // Uses API to correctly manage CORS
    app.use(cors({
        origin: config.corsUrl,
        credentials : true
    }));

    // XSS sanitization middleware
    app.use(xssSanitizer);

    // Setup the routes for the application
    setupUsersRoutes(app);
    setupAuthRoutes(app);
    setupTransactionRoutes(app);

    // Setup static asset serving
    app.use('/', express.static('../client/build'));

    return app;
}