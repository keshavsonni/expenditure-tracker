import { MongoClient, Db, MongoError, Collection } from 'mongodb';

import { config } from '../config/config';
import { DbCollection } from '../library/models/db';
import { setupConstraints as setupUserConstraints } from '../library/users/usersManager';

const url = `mongodb://localhost`;

let db: Db = null;

// Helper functions to connect to MongoDB
export const connectDb = function(): Promise<void> {
    return new Promise((resolve, reject) => {
        MongoClient.connect(url, (error: MongoError, client: MongoClient) => {
            if (!error) {
                console.log(`Connected to MongoDB server successfuly`);
            } else {
                reject('Failed to connect to MongoDB');
            }

            db = client.db(config.dbName);
            resolve();
        });
    });
}

export const getDbCollection = function(collection: DbCollection): Collection {
    return db.collection(collection); 
}

export const setupDb = function(): Promise<void> {
    return new Promise( async (resolve, reject) => {
        await connectDb();
        await setupUserConstraints();
    });
}