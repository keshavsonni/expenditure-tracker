import { AuthMethod } from './models';
import { Request, Response } from 'express';
import { httpError } from '../../library/utils/http';
import { setMiddlewareResult } from '../../library/models/middleware';
import { isEmpty } from '../../library/utils';

// Validate the input supplied for validation and exit early
export const validateAuthMechanism = function(req: Request, res: Response, next: () => void) {
    const authHeader = req.get('authorization');
    
    if(!isEmpty(req.cookies.token)) {
        setMiddlewareResult(req, 'authMethod', {
            type: 'cookie',
            token: req.cookies.token
        });
        return next();
    }

    if(!authHeader) 
        return httpError.authFailed(res);
    
    const credentials = Buffer.from(authHeader.split(' ').pop(), 'base64')
                        .toString('ascii').split(':');
    
    if(!credentials || credentials.length !== 2 || 
        isEmpty(credentials[0]) || isEmpty(credentials[1])) 
        return httpError.authFailed(res);

    const method: AuthMethod = {
        type: 'header',
        username: credentials[0],
        password: credentials[1]
    };

    setMiddlewareResult(req, 'authMethod', method);    
    next();
}