import { Application } from 'express';
import { authenticate, login, logout } from './authController';
import { validateAuthMechanism } from './requestValidator'
import { config } from '../../config/config';

// Authentication routes
export const setupRoutes = function(app: Application) {
    app.route(`${config.apiPath}login`).post(validateAuthMechanism, authenticate, login);
    app.route(`${config.apiPath}logout`).post(logout);
}