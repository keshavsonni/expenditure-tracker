// All types to do with authentication
import { User } from "../../library/models/db";

export type AuthMethodType = 'cookie' | 'header';

export interface BaseAuthMethod {
    type: AuthMethodType;
}

export interface CookieAuthMethod extends BaseAuthMethod {
    type: 'cookie';
    token: string;
}

export interface HeaderAuthMethod extends BaseAuthMethod {
    type: 'header';
    username: string;
    password: string;
}

export type AuthMethod = CookieAuthMethod | HeaderAuthMethod;

export type AuthUser = Omit<Omit<User, 'transactions'>, 'password'>;

export type TokenType = 'login';

export interface AccessTokenData {
    email: string;
    type: TokenType;
};

export type CookieName = keyof CookieFields;

export interface CookieFields {
    token: string;
}
