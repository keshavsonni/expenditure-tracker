import { Request, Response } from 'express';
import { getMiddlewareResult, setMiddlewareResult } from '../../library/models/middleware';
import { httpError } from '../../library/utils/http';
import { HeaderAuthMethod, CookieAuthMethod, AccessTokenData, CookieName } from './models';
import { findUsers } from '../../library/users/usersManager';
import { comparePasswords, generateAccessToken, verifyAccessToken } from '../../library/utils/crypto';
import { config } from '../../config/config';
import { isDev } from '../../library/utils';

// Decide auth method 
export const authenticate = async function(req: Request, res: Response, next?: () => void) {
    const method = getMiddlewareResult(req, 'authMethod');

    switch(method?.type) {
        case 'header':
            return await basicAuthentication(method, req, res, next);
        case 'cookie':
            cookieAuthentication(method, req, res, next);
            break;    
        default:
            return httpError.authFailed(res);
    }
}

// Usernme/password auth
export const basicAuthentication = async function(method: HeaderAuthMethod, req: Request, res: Response, next: () => void) {
    const users = await findUsers({
        email: method.username
    }, { transactions: 0 });

    if(!users || users.length === 0) {
        return httpError.authFailed(res);
    }
    const user = users[0];
    const success = await comparePasswords(method.password, user.password);
    if(!success)
        return httpError.authFailed(res);

    const tokenData :AccessTokenData = {
        email: user.email,
        type: 'login'
    };
    
    const token = generateAccessToken(tokenData,config.jwtLifeSeconds,config.jwtSecret);
    const cookieName: CookieName = 'token';
    
    res.cookie(cookieName, token, {
        httpOnly: false,
        secure: false,
        sameSite: false
    });

    setMiddlewareResult(req, 'authUser', user);
    next();
}

// Cookie based auth
export const cookieAuthentication = async function(method: CookieAuthMethod, req: Request, res: Response, next: () => void) {
    const verification = verifyAccessToken(method.token, config.jwtSecret) as AccessTokenData;
    if(!verification) {
        return httpError.authFailed(res);
    }
    const users = await findUsers({
        email: verification.email
    }, { transactions: 0, password: 0 });

    if(!users || users.length === 0) {
        return httpError.authFailed(res);
    }
    const user = users[0];
    setMiddlewareResult(req, 'authUser', user);
    next();
}

export const login = function(req: Request, res: Response) {
    res.sendStatus(200);
}

export const logout = function(req: Request, res: Response) {
    const cookieName: CookieName = 'token';
    res.clearCookie(cookieName);
    res.sendStatus(200);
}