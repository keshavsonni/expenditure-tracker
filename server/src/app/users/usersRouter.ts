import { Application } from 'express';
import { createUser, getUser, updateUser, deleteUser } from './usersController';
import { validateCreateUser, validateUpdateUser } from './requestValidator'
import { config } from '../../config/config';
import { authenticate, logout } from '../auth/authController';
import { validateAuthMechanism } from '../auth/requestValidator';

export const usersRoute = config.apiPath + 'user';

export const setupRoutes = function(app: Application) {
    // API requests to the /users subpath
    app.route(`${usersRoute}`).post(validateCreateUser, createUser)
                              .get(validateAuthMechanism, authenticate, getUser)
                              .put(validateAuthMechanism, authenticate, validateUpdateUser, updateUser)
                              .delete(validateAuthMechanism, authenticate, deleteUser, logout);
}