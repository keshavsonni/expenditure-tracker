import { User } from "../../library/models/db"
import { Request, Response } from 'express';
import { httpError } from '../../library/utils/http';
import { isValidEmail, isEmpty } from "../../library/utils";

// Validation before moving into the controller
export const validateCreateUser = function(req: Request, res: Response, next: () => void) {
    const user: Partial<User> = req.body as Partial<User>;
    
    if(!user.name || !user.email || !user.password) {
        return httpError.missingParameters(res);
    }
    if(!isValidEmail(user.email)) {
        return httpError.invalidValue<User>(res, 'email');
    }
    if(user.name.length > 40) {
        return httpError.invalidValue<User>(res, 'name', { maxChars: 40 });
    }
    if(user.password.length < 8) {
        return httpError.invalidValue<User>(res, 'password', { minChars : 8 });
    }

    next();
}

export const validateUpdateUser = function(req: Request, res: Response, next: () => void) {
    const user: Partial<User> = req.body as Partial<User>;
    
    if(isEmpty(user.name)) {
        return httpError.missingParameters(res);
    }

    if(user.name.length > 40) {
        return httpError.invalidValue<User>(res,'name', {maxChars: 40} )
    }

    next();
}