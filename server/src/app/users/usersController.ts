import { Request, Response } from 'express';
import { createUser as createDbUser, updateUser as updateDbUser, deleteUser as deleteDbUser } from '../../library/users/usersManager';
import { User } from '../../library/models/db';
import { httpError } from '../../library/utils/http';
import { getMiddlewareResult } from '../../library/models/middleware';
import { prettyId } from '../../library/utils';
// CRUD operations on users
export const createUser = async function(req: Request, res: Response, next: () => void) {
    const user = req.body as User;
    
    try {
        await createDbUser(user);
    } catch (e) {
        console.error(`Failed to create user: ${e.errmsg}`);
        return httpError.forbiddenOperation(res);
    }

    res.send('Successfully created user');
}

export const getUser = async function(req: Request, res: Response, next: () => void) {
    const user = getMiddlewareResult(req, 'authUser');
    res.json(prettyId(user));
}

export const updateUser = async function(req: Request, res: Response, next: () => void) {
    const user = getMiddlewareResult(req, 'authUser');
    const reqUser = req.body as User;
    
    try {
        await updateDbUser(user._id, {
            name: reqUser.name
        });
    } catch(e) {
        console.error(`Failed to update user: ${e.errmsg}`);
        return httpError.failedOperation(res);
    }

    return res.sendStatus(200);
}

export const deleteUser = async function(req: Request, res: Response, next: () => void) {
    const user = getMiddlewareResult(req, 'authUser');

    try {
        await deleteDbUser(user._id);
    } catch(e) {
        console.error(`Failed to delete user: ${e.errmsg}`);
        return httpError.failedOperation(res);
    }

    next();
}