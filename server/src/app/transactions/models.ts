// All types to do with transactions
import { Transaction } from "../../library/models/db";

export const getTransactionQueryKeys : Record<GetTransactionQueryKey, string> = {
    transactionId: 'id',
    description: 'description',
    from: 'from',   // Start time
    to: 'to',       // End time
    page: 'page',
    results: 'results',
    min: 'min',     // Min value
    max: 'max',     // Max value
    category: 'category'
}

export const deleteTransactionQueryKeys : Record<DeleteTransactionQueryKey, string> = {
    transactionId: 'id'
}

export const updateTransactionQueryKeys : Record<UpdateTransactionQueryKey, string> = {
    id: 'id',
    category: 'category',
    description: 'description',
    value: 'value'
}

export const getCategoriesReportQueryKeys : Record<GetCategoriesReportQueryKey, string> = {
    from: 'from',
    to: 'to'
}

type GetTransactionQueryKey = keyof Omit<GetTransactionByIdQuery, 'kind'> | keyof Omit<GetTransactionByFieldsQuery, 'kind'>;

export type GetTransactionQuery = GetTransactionByIdQuery | GetTransactionByFieldsQuery;

export interface GetTransactionByIdQuery {
    kind: 'id';
    transactionId: string;
}

export interface GetTransactionByFieldsQuery {
    kind: 'fields';
    from?: number;
    to?: number;
    results?: number;
    page?: number;
    description?: string;
    min?: number;
    max?: number;
    category?: number;
}

export type DeleteTransactionQueryKey = keyof DeleteTransactionQuery;

export interface DeleteTransactionQuery {
    transactionId: string;
}

export type UpdateTransactionQueryKey = keyof UpdateTransactionQuery;

export type UpdateTransactionQuery = Omit<Partial<Transaction>, 'createdAt'>;

export type GetCategoriesReportQueryKey = keyof GetCategoriesReportQuery;

export interface GetCategoriesReportQuery  {
    from?: number;
    to?: number;
}

export type CategoriesReport = Record<number, number>;