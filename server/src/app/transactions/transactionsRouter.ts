import { Application } from 'express';
import { config } from '../../config/config';
import { authenticate } from '../auth/authController';
import { createTransaction, getTransactions, deleteTransaction, updateTransaction, getCategories, getCategoriesReport, getDailyReport, getMonthlyReport } from './transactionsController';
import { validateAuthMechanism } from '../auth/requestValidator';
import { validateCreateTransaction, validateGetTransaction, validateDeleteTransaction, validateUpdateTransaction, validateGetCategoriesReport } from './requestValidator';
import { deleteTransactionQueryKeys, updateTransactionQueryKeys, getTransactionQueryKeys } from './models';

export const transactionsRoute = config.apiPath + 'transactions';
export const categoriesRoute = config.apiPath + 'categories';

export const setupRoutes = function(app: Application) {
    // API requests to the /transactions subpath
    app.route(`${transactionsRoute}`).post(validateAuthMechanism, authenticate, validateCreateTransaction, createTransaction);

    app.route(`${transactionsRoute}`).get(validateAuthMechanism, authenticate, validateGetTransaction, getTransactions);
    
    app.route(`${transactionsRoute}/:${getTransactionQueryKeys.transactionId}`).get(validateAuthMechanism, authenticate, 
                                    validateGetTransaction, getTransactions);

    app.route(`${transactionsRoute}/:${deleteTransactionQueryKeys.transactionId}`).delete(validateAuthMechanism, authenticate, 
                                    validateDeleteTransaction, deleteTransaction);
                                    
    app.route(`${transactionsRoute}/:${updateTransactionQueryKeys.id}`).put(validateAuthMechanism, authenticate, 
                                    validateUpdateTransaction, updateTransaction);

    app.route(`${categoriesRoute}`).get(validateAuthMechanism, authenticate, getCategories);

    app.route(`${categoriesRoute}/report`).get(validateAuthMechanism, authenticate, validateGetCategoriesReport, getCategoriesReport);
    
    app.route(`${config.apiPath}dailyReport`).get(validateAuthMechanism, authenticate, validateGetTransaction, getDailyReport);

    app.route(`${config.apiPath}monthlyReport`).get(validateAuthMechanism, authenticate, validateGetTransaction, getMonthlyReport);
}