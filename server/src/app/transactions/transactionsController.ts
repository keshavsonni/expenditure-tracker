import { Request, Response } from 'express';
import { httpError } from '../../library/utils/http';
import { getMiddlewareResult } from '../../library/models/middleware';
import { Transaction } from '../../library/models/db';
import { createTransaction as createDbTransaction, findTransaction, listTransactions, 
        deleteTransaction as deleteDbTransaction, updateTransaction as updateDbTransaction,
        getCategoriesReport as getDbCategoriesReport, aggregateTransactionsByDay, aggregateTransactionsByMonth
} from '../../library/transactions/transactionManager';
import { GetTransactionByIdQuery, GetTransactionByFieldsQuery, DeleteTransactionQuery, UpdateTransactionQuery, GetCategoriesReportQuery, CategoriesReport } from './models';
import { AuthUser } from '../auth/models';
import { listCategories } from '../../library/transactions/categoryManager';
import { PaginatedResult } from '../../library/utils';
import { updateTotalTransactionsForUser } from '../../library/users/usersManager';
import { config } from '../../config/config';

// Database functions to query transactions
export const createTransaction = async function(req: Request, res: Response, next: () => void) {
    const user = getMiddlewareResult(req, 'authUser');

    if (user.totalTransactions >= config.maxTransactionsPerUser) {
        return httpError.insufficientStorage(res, 'Max transactions limit reached');
    }

    const transaction = req.body as Transaction;

    try {
        const result = await createDbTransaction(user._id, {
            description: transaction.description,
            value: transaction.value,
            category: transaction.category
        });
        await updateTotalTransactionsForUser(user._id);
        return res.json(result);
    } catch (e) {
        console.error(`Failed to create transaction: ${e.errmsg}`);
        return httpError.failedOperation(res);
    }
}

export const getDailyReport = async function(req: Request, res: Response, next: () => void) {
    const user = getMiddlewareResult(req, 'authUser');
    const query = getMiddlewareResult(req, 'getTransactionQuery') as GetTransactionByFieldsQuery;

    try {
        const results = await listTransactions(user._id, query) as PaginatedResult<Transaction>;
        if(results == null) {
            return httpError.notFound(res);
        }
        const report = aggregateTransactionsByDay(results.result);
        res.json(report);
    } catch (e) {
        console.error(`Failed to get daily report: ${e.errmsg}`);
        return httpError.failedOperation(res);
    }
}

export const getMonthlyReport = async function(req: Request, res: Response, next: () => void) {
    const user = getMiddlewareResult(req, 'authUser');
    const query = getMiddlewareResult(req, 'getTransactionQuery') as GetTransactionByFieldsQuery;

    try {
        const results = await listTransactions(user._id, query) as PaginatedResult<Transaction>;
        if(results == null) {
            return httpError.notFound(res);
        }
        const report = aggregateTransactionsByMonth(results.result);
        res.json(report);
    } catch (e) {
        console.error(`Failed to get daily report: ${e.errmsg}`);
        return httpError.failedOperation(res);
    }
}

export const getTransactions = async function(req: Request, res: Response, next: () => void) {
    const user = getMiddlewareResult(req, 'authUser');
    const query = getMiddlewareResult(req, 'getTransactionQuery');

    switch (query.kind) {
        case 'id':
            getTransactionById(query, user, res, next);
            break;
        case 'fields':
            getTransactionsByField(query, user, res, next);
            break;
    }
}

const getTransactionById = async function(query: GetTransactionByIdQuery, user: AuthUser, res: Response, next: ()=> void) {
    try {
        const result = await findTransaction(user._id, query.transactionId);
        if(result == null) {
            return httpError.notFound(res);
        }
        return res.json(result);
    } catch (e) {
        console.error(`Failed to get transactions: ${e.errmsg}`);
        return httpError.failedOperation(res);
    }
}

const getTransactionsByField = async function(query: GetTransactionByFieldsQuery, user: AuthUser, res: Response, next: ()=> void) {
    try {
        const results = await listTransactions(user._id, query);
        if(results == null) {
            return httpError.notFound(res);
        }
        return res.json(results);
    } catch (e) {
        console.error(`Failed to get transaction: ${e.errmsg}`);
        return httpError.failedOperation(res);
    }
}

export const deleteTransaction = async function(req: Request, res: Response, next: () => void) {
    const authUser : AuthUser = getMiddlewareResult(req, 'authUser');
    const transactionReq : DeleteTransactionQuery = getMiddlewareResult(req, 'deleteTransactionQuery');
    try {
        await deleteDbTransaction(authUser._id, transactionReq.transactionId);
        await updateTotalTransactionsForUser(authUser._id);
        return res.sendStatus(200);
    } catch (e) {
        console.error(`Failed to delete transaction: ${e.errmsg}`);
        return httpError.failedOperation(res);
    }
}

export const updateTransaction = async function(req: Request, res: Response, next: () => void) {
    const authUser : AuthUser = getMiddlewareResult(req, 'authUser');
    const transactionReq : UpdateTransactionQuery = getMiddlewareResult(req, 'updateTransactionQuery');
    try {
        await updateDbTransaction(authUser._id, transactionReq);
        return res.sendStatus(200);
    } catch (e) {
        console.error(`Failed to update transaction: ${e.errmsg}`);
        return httpError.failedOperation(res);
    }
}

export const getCategories = async function(req: Request, res: Response, next: () => void) {
    res.json(listCategories());
}

export const getCategoriesReport = async function(req: Request, res: Response, next: () => void) {
    const authUser : AuthUser = getMiddlewareResult(req, 'authUser');
    const query : GetCategoriesReportQuery = getMiddlewareResult(req, 'getCategoriesReportQuery');
    
    try {
        const report : CategoriesReport = await getDbCategoriesReport(authUser._id, query);
        res.json(report);
    } catch (e) {
        console.error(e);
        httpError.failedOperation(res);
    }
}