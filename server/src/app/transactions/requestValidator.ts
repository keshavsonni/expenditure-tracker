import { Request, Response } from 'express';
import { Transaction } from '../../library/models/db';
import { isEmpty, isNonEmptyNaN } from '../../library/utils';
import { httpError } from '../../library/utils/http';
import { isUuid } from 'uuidv4';
import { getTransactionQueryKeys, GetTransactionQuery, GetTransactionByFieldsQuery, deleteTransactionQueryKeys, DeleteTransactionQuery, updateTransactionQueryKeys, UpdateTransactionQueryKey, UpdateTransactionQuery, getCategoriesReportQueryKeys, GetCategoriesReportQuery } from './models';
import { setMiddlewareResult } from '../../library/models/middleware';
import { isValidCategoryId } from '../../library/transactions/categoryManager';

// Ensure user supplies valid input
export const validateCreateTransaction = function(req: Request, res: Response, next: () => void) {
    const transaction = req.body as Partial<Transaction>;
    if (!transaction || !transaction.value || !transaction.category) {
        return httpError.missingParameters(res);
    }

    if(!isEmpty(transaction.description) && transaction.description.length > 80 ) {
        return httpError.invalidValue<Transaction>(res, 'description', { maxChars: 80 });
    }

    if(transaction.value != null && transaction.value < 0) {
        return httpError.invalidValue<Transaction>(res, 'value', { minVal: 0 });
    }

    
    if(isNaN(transaction.category) || !isValidCategoryId(transaction.category)) {
        return httpError.invalidValue<Transaction>(res, 'category');
    }

    next();
}

// Validate get queries sent by the user
export const validateGetTransaction = function(req: Request, res: Response, next: () => void) {
    const transactionId: string = req.params[getTransactionQueryKeys.transactionId];
    const results: number = req.query[getTransactionQueryKeys.results] != null ? parseInt(req.query[getTransactionQueryKeys.results]) : null;
    const from: number = req.query[getTransactionQueryKeys.from] != null ? parseInt(req.query[getTransactionQueryKeys.from]) : null;
    const to: number = req.query[getTransactionQueryKeys.to] != null ? parseInt(req.query[getTransactionQueryKeys.to]) : null;
    const page: number = req.query[getTransactionQueryKeys.page] != null ? parseInt(req.query[getTransactionQueryKeys.page]) : null;
    const min: number = req.query[getTransactionQueryKeys.min] != null ? parseFloat(req.query[getTransactionQueryKeys.min]) : null;
    const max: number = req.query[getTransactionQueryKeys.max] != null ? parseFloat(req.query[getTransactionQueryKeys.max]) : null;
    const description: string = req.query[getTransactionQueryKeys.description];
    const category: number = req.query[getTransactionQueryKeys.category] != null ? parseInt(req.query[getTransactionQueryKeys.category]) : null;
    
    if(!isEmpty(transactionId)) {
        if(!isUuid(transactionId)) {
            return httpError.badRequest(res);
        }
        const transactionQuery: GetTransactionQuery = {
            kind: 'id',
            transactionId: transactionId
        }
        setMiddlewareResult(req, 'getTransactionQuery', transactionQuery);
        return next();
    }

    if(isNonEmptyNaN(results) || isNonEmptyNaN(from) || isNonEmptyNaN(to) || isNonEmptyNaN(page) || isNonEmptyNaN(min) || isNonEmptyNaN(max)
        || isNonEmptyNaN(category)) {
        return httpError.badRequest(res);
    }

    if (results != null && results < 1) return httpError.invalidValue<GetTransactionByFieldsQuery>(res, 'results', {minVal: 1});
    if (from != null && from < 0) return httpError.invalidValue<GetTransactionByFieldsQuery>(res, 'from', {minVal: 0});
    if (to != null && to < 0) return httpError.invalidValue<GetTransactionByFieldsQuery>(res, 'to', {minVal: 0});
    if (page != null && page < 1) return httpError.invalidValue<GetTransactionByFieldsQuery>(res, 'page', {minVal: 1});
    if (min != null && min < 0) return httpError.invalidValue<GetTransactionByFieldsQuery>(res, 'min', {minVal: 0});
    if (max != null && max < 0) return httpError.invalidValue<GetTransactionByFieldsQuery>(res, 'max', {minVal: 0}); 
    if (description != null && (description.length == 0 || description.length > 40)) 
        return httpError.invalidValue<GetTransactionByFieldsQuery>(res, 'description', {minChars: 1, maxChars: 40});    
    if (category != null && !isValidCategoryId(category)) return httpError.invalidValue<GetTransactionByFieldsQuery>(res, 'category'); 
    
    const query: GetTransactionQuery = {
        kind: 'fields',
        description: description,
        from: from,
        to: to,
        page: page,
        results: results,
        max: max,
        min: min,
        category: category
    };

    setMiddlewareResult(req, 'getTransactionQuery', query);
    next();
}

export const validateDeleteTransaction = function(req: Request, res: Response, next: () => void) {
    const transactionId: string = req.params[deleteTransactionQueryKeys.transactionId];
    if(isEmpty(transactionId)) {
        return httpError.missingParameters(res);
    }
    if(!isUuid(transactionId)) {
        return httpError.badRequest(res);
    }

    const query : DeleteTransactionQuery = {
        transactionId: transactionId
    };
    setMiddlewareResult(req, 'deleteTransactionQuery', query);
    
    next();
}

export const validateUpdateTransaction = function(req: Request, res: Response, next: () => void) {
    const transactionId: string = req.params[updateTransactionQueryKeys.id];
    if(isEmpty(transactionId)) {
        return httpError.missingParameters(res);
    }
    if(!isUuid(transactionId)) {
        return httpError.badRequest(res);
    }

    const description: string = req.body[updateTransactionQueryKeys.description];
    const value: number = req.body[updateTransactionQueryKeys.value] != null ? parseFloat(req.body[updateTransactionQueryKeys.value]) : null;
    const category: number = req.body[updateTransactionQueryKeys.category] != null ? parseInt(req.body[updateTransactionQueryKeys.category]) : null;

    if(isEmpty(description) && value == null) {
        return httpError.missingParameters(res);
    }
    if(isNonEmptyNaN(value) || isNonEmptyNaN(category)) {
        return httpError.badRequest(res);
    }
    
    if(description != null && (description.length == 0 || description.length > 40)) 
        return httpError.invalidValue<UpdateTransactionQuery>(res, 'description', {minChars: 1, maxChars: 40});
    if(value != null && value < 0) return httpError.invalidValue<UpdateTransactionQuery>(res, 'value', {minVal: 0});
    if(category != null && !isValidCategoryId(category)) return httpError.invalidValue<UpdateTransactionQuery>(res, 'category');

    const query : UpdateTransactionQuery = {
        description: description,
        value: value,
        id: transactionId,
        category: category
    };
    setMiddlewareResult(req, 'updateTransactionQuery', query);

    next();
}

// Validate category reporting query
export const validateGetCategoriesReport = function(req: Request, res: Response, next: () => void) {
    const from = req.query[getCategoriesReportQueryKeys.from] != null ? parseInt(req.query[getCategoriesReportQueryKeys.from]) : null;
    const to = req.query[getCategoriesReportQueryKeys.to] != null ? parseInt(req.query[getCategoriesReportQueryKeys.to]) : null;

    if(isNonEmptyNaN(from) || isNonEmptyNaN(to)) {
        return httpError.badRequest(res);
    }

    if(from != null && from < 0) return httpError.invalidValue<GetCategoriesReportQuery>(res, 'from', {minVal: 0});
    if(to != null && to < 0) return httpError.invalidValue<GetCategoriesReportQuery>(res, 'to', {minVal: 0});

    const query : GetCategoriesReportQuery = {
        from : from,
        to: to
    }

    setMiddlewareResult(req, 'getCategoriesReportQuery', query);

    next();
}