# Expenditure tracker

## Intro
A web app to log transactions, track expenses and get a good understanding with visualisation tools. 

## Technical description
### Frontend (work in progress)
- The frontend has been written with **React** & **Redux**
- Strong type checking with **TypeScript**
- Leverages **React hooks** with fully **functional components**
- Uses an all **custom component library** to exhibit component design (including the data table)
- Uses **SCSS** & **Flexbox** for styling
- **Block Element Modifier (BEM)** methodology to organise CSS
- **Mobile support**
- Tests still are being added

### Backend 
- **NodeJS Express** app written with **TypeScript** with a strong focus on static typing
- Uses **MongoDB NoSQL** for data storage
- Uses **JSON Web tokens** for authentication
- **REST API** with CRUD operations for user management, transactions and constructing advanced filters (sill needs UI)
- The filtering section being complicated, has been tested with **Jest**. Coverage will be improved in the future 




### Demo
Note that the backend API is functional but the UI is still Work In Progress. Support for filters and generating reports exists but needs to be implemented in the UI. This provisions Guest access
[https://okadadigital.com/?token=mpmllfrcobsqjldew](http://okadadigital.com/?token=mpmllfrcobsqjldew)

(P.S: Okada digital is just my cyber identity as my full name domain had been taken)